﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using SharedLib;
using Newtonsoft.Json;

namespace Correctloggingdata
{
    static public class DatabaseIO
    {

        static readonly private string _MyConnString = "server=vs-heditmigratie1.edit.nos.nl;uid=itbo;pwd=diwemww11;database=inosmam_migratie;UseAffectedRows=True;SslMode=None";

        static private bool Open(MySqlConnection Connection)
        {
            try
            {
                Connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Open Mysql connection: {ex.Message}");
                return false;
            }
        }




        static public List<Asset> GetAssets()
        {
            string Query = "SELECT * FROM assets WHERE asset_deleted = 0 and logging_metadata is not null";

            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return null;

                using (var sc = new MySqlCommand(Query, sq))
                {
                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        List<Asset> AssetList = new List<Asset>();
                        while (mySQlReader.Read())
                        {
                            Asset asset = new Asset() { Assetdata = new AssetData() };
                            asset.AssetID = Convert.ToInt64(mySQlReader["itx_asset_id"]);
                            asset.Assetdata.AssetName = mySQlReader["guci"].ToString();
                            asset.Assetdata.Inpoint = mySQlReader.GetString("inpoint");
                            asset.Assetdata.IsPlaceholder = Convert.ToBoolean(mySQlReader["is_placeholder"]);
                            asset.Assetdata.Duration = mySQlReader.GetString("duration");
                            asset.AssetDeleted = Convert.ToBoolean(mySQlReader["asset_deleted"]);
                            asset.Assetdata.LastModified = Convert.ToDateTime(mySQlReader["date_last_modified"]);
                            asset.Assetdata.Logevents = JsonConvert.DeserializeObject<List<LoggingMetadata>>(mySQlReader.GetString("logging_metadata")).ToArray();
                            AssetList.Add(asset);
                        }
                        return AssetList;
                    }
                }
            }
        }
    }
}
