﻿using System;
using System.ServiceProcess;

namespace Windows_Service_ITX_InsertTo_DB
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var service = new ServiceStarter(""))
            {
                ServiceBase.Run(service);
            }
        }
    }
}
