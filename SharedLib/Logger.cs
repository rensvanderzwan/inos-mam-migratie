﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace SharedLib
{
    public class RabbitLoggingHost
    {
        public int Id { get; set; }
        public string INOS_environment { get; set; }
        public string Host { get; set; }
        public string Vhost { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Exchange { get; set; }
        public string Routingkey { get; set; }
        public string AppID { get; set; }
    }

    public class RabbitLogMsg
    {
        public string MsgType { get; set; }
        public string Location { get; set; }
        public string ProcessName { get; set; }
        public string RefId { get; set; }
        public string Msg { get; set; }
        public string MsgDetail { get; set; }
        public long Timestamp { get; set; }
    }

    public enum MsgType
    {
        INFO,
        WARN,
        ERROR,
        DEBUG
    }

    public class Logger
    {
        readonly private static ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();
        public static string _LogPath { get; set; }
        public static IConnection Connection;
        public static IModel Channel;
        public static string _HostName;
        public static string _VirtualHost;
        public static string _UserName;
        public static string _Password;
        public static string _Exchange;
        public static string _RoutingKey;
        public static string _ProcessName;
        public static string _Location;
        public static string _AppId = "Bronnen Service";


        public static bool Connect(RabbitLoggingHost host)
        {
            if (Connection != null && Connection.IsOpen && Channel.IsOpen) return true;

            _HostName = host.Host;
            _VirtualHost = host.Vhost;
            _UserName = host.Login;
            _Password = host.Password;
            _Exchange = host.Exchange;
            _RoutingKey = host.Routingkey;
            _ProcessName = host.AppID;
            _Location = "hlv";
            _RoutingKey = host.Routingkey;

            var factory = new ConnectionFactory() { HostName = _HostName, VirtualHost = _VirtualHost, UserName = _UserName, Password = _Password };
            factory.AutomaticRecoveryEnabled = true;
            //factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);

            try
            {
                Connection = factory.CreateConnection();
                Channel = Connection.CreateModel();
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"RabbitConnector: {ex.Message}, {ex.InnerException}");
                return false;
            }
        }


        public static void Disconnect()
        {
            try
            {
                if (Channel != null && Channel.IsOpen)
                {
                    Channel.Close();
                    Channel = null;
                }

                if (Connection != null && Connection.IsOpen)
                {
                    Connection.Close();
                    Connection = null;
                }
            }
            catch (IOException)
            {
                // Close() may throw an IOException if connection dies - but that's ok
            }
        }



        public static IBasicProperties CreateBasicProperties(string Type)
        {
            try
            {
                var properties = Channel.CreateBasicProperties();
                properties.AppId = _AppId;
                properties.Type = Type;
                properties.MessageId = "12345";
                properties.ReplyTo = "";
                properties.CorrelationId = "";
                Dictionary<string, object> Headers = new Dictionary<string, object>();
                Headers.Add("time", DateTime.UtcNow.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ"));
                properties.Headers = Headers;

                return properties;
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "CreateBasicProperties: " + ex.Message);
                return null;
            }

        }



        public static void RotateLog()
        {
            if (File.Exists(_LogPath))
            {
                FileInfo f = new FileInfo(_LogPath);
                if (f.Length > 5 * 1024 * 1024)
                {
                    string ArchiveName = _LogPath + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".log";
                    File.Delete(ArchiveName); // Delete the existing file if exists
                    File.Move(_LogPath, ArchiveName);
                    WriteLog(MsgType.INFO, "--", $"Log Rotation: {ArchiveName}");
                }
            }
        }


        public static void WriteLog(MsgType LogType, string refID, string Message)
        {
            string Reference = refID ?? "---";
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_LogPath));
            try
            {
                using (StreamWriter w = File.AppendText(_LogPath))
                {
                    w.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {Reference.PadLeft(6, ' ')} {LogType.ToString()}: {Message}");
                    w.Close();
                }
            }
            catch (Exception)
            {
                // How to log this exeception?
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }

        public static void WriteConsoleAndLog(MsgType LogType, string refID, string Message)
        {
            string Reference = refID ?? "---";
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_LogPath));
            try
            {
                using (StreamWriter w = File.AppendText(_LogPath))
                {
                    w.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {Reference.PadLeft(6, ' ')} {LogType.ToString()}: {Message}");
#if DEBUG
                    Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {Reference.PadLeft(6, ' ')} {Message}");
#endif
                }
            }
            catch (Exception)
            {
                // How to log this exeception?
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }

        public static void WriteConsoleAndLogAndBus(MsgType LogType, string refID, string Message, string DetailMessage = "")
        {
            string Reference = refID ?? "---";
            WriteConsoleAndLog(LogType, Reference, Message);

            RabbitLogMsg msg = new RabbitLogMsg
            {
                MsgType = LogType.ToString(),
                Location = _Location,
                ProcessName = _ProcessName,
                RefId = Reference,
                Msg = Message == null ? null : Message.Substring(0, Math.Min(Math.Abs(511), Message.Length)),
                MsgDetail = DetailMessage,
                Timestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds * 1000
            };

            string message = JsonConvert.SerializeObject(msg);
            var body = Encoding.UTF8.GetBytes(message);

            try
            {
                Channel.BasicPublish(exchange: _Exchange,
                                     routingKey: _RoutingKey,
                                     mandatory: false,
                                     basicProperties: CreateBasicProperties("logging"),
                                     body: body);
            }
            catch (Exception ex)
            {
                WriteConsoleAndLog(MsgType.ERROR, "", $"Error LogtoBus: {ex.Message}");
                // No need to reconnect here, Is done autmatically for the next loop.
            }
        }
    }
}
