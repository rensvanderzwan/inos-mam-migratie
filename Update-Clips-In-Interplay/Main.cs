﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using SharedLib;

// Update metadata of Interplay clips with changed metadata from database.
// Flag them as deleted if they are deleted in ITX.
// Move clips to another folder if year of departement has changed.
// Add and change locator data 


// Make some database functions to collect clips.
// Retrieve master clips from "Archief" Interplay folder.
// Move clip to another folder if Contenttype or Division has changed.
// Correct Contentype , Vervaldatum and Verhaaldatum if changed.
// Set iNOS Housekeeping to T-0 if Interplay clip is not present in database list.

namespace Update_Clips_In_Interplay
{
    class Main
    {
        private string _archiveFolder = "Catalogs/iNOS-MAM Archief";
        public Main()
        {
            // don't forget to switch to another webservice for Den Haag.
            // Remember we're using the remote database , cause the project is run manually.
            string location = "dhg";  // tst, hlv or dhg;
            Logger._LogPath = $@"E:\Logs\Update-InterplayClips_{location}.log";
            Logger.WriteConsoleAndLog(MsgType.INFO, null, $"############### Start App, location: {location} ###############");


            InterplayService.Init(location);
            // Get all assets (masterclip) from Interplay archive folder
            List<Asset> interplayAssets = InterplayService.GetArchiveClips2(InterplayService._interplayUri +  _archiveFolder, new List<Asset>());

      //      MakeCompareList(interplayAssets, location);

            UpdateFileSizes(interplayAssets, location);

            CheckForAnomalies(interplayAssets, location);

            UpdateAttributes(interplayAssets, location);
     
            FlagDeletedClips(interplayAssets, location);

 //           UpdateLocators(interplayAssets, location);

            MoveToFolder(interplayAssets, location);

            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Finish App: ###############");
        }

        private void MakeCompareList(List<Asset> interplayAssets, string location)
        {
            //remove assets allready having housekeeping set to t-0
            List<Asset> filteredInterplayAssets = interplayAssets.Where(x => x.AssetDeleted != true).ToList();


            string AssetIDs = String.Join("\r\n", filteredInterplayAssets.Select(x => x.AssetID));
            File.WriteAllText($@"E:\Logs\FilteredInterplayClips_{location}.log", AssetIDs);

            List<Asset> databaseAssets = DatabaseIO.GetAssets(false, location);
            if (databaseAssets == null)
            {
                return;
            }
            // get assets that should be present in Interplay
            List<Asset> filteredDatabaseAssets = databaseAssets.Where(x => x.AssetDeleted == false && x.Assetdata.IsPlaceholder == false && x.IngestStatus == "finished").ToList();

            // get interplay assets not present in filtered assets
            List<Asset> todoAssets = filteredDatabaseAssets.Where(x => !filteredInterplayAssets.Any(y => (y.AssetID == x.AssetID))).ToList();

            List<Asset> SurplusAssets = filteredInterplayAssets.Where(x => !filteredDatabaseAssets.Any(y => (y.AssetID == x.AssetID))).ToList();

            string todoAssetIDs = String.Join("\r\n", todoAssets.Select(x => x.AssetID));
            File.WriteAllText($@"E:\Logs\TodoClips_{location}.log", todoAssetIDs);

            string surplusAssetIDs = String.Join("\r\n", SurplusAssets.Select(x => x.AssetID));
            File.WriteAllText($@"E:\Logs\SurplusClips_{location}.log", surplusAssetIDs);

        }
        private void UpdateFileSizes(List<Asset> interplayAssets, string location)
        {
            List<Asset> databaseAssets = DatabaseIO.GetAssets(false, location);
            if (databaseAssets == null)
            {
                return;
            }
            // select only the ones that have no InterplaysizeBytes yet and not running.
            List<Asset> assetsToDo = databaseAssets.Where(x => x.InterplaysizeBytes == null && x.InterplayMobID != null && x.IngestStatus != "running").ToList();

            foreach(Asset asset in assetsToDo)
            {
                long size = InterplayService.GetFileDetails(asset.InterplayMobID);
                DatabaseIO.UpdateAsset(new Asset() { AssetID = asset.AssetID, Assetdata = new AssetData(), InterplaysizeBytes = size, IngestLocation = location });
            }

        }

        private void UpdateBackupStatus(List<Asset> interplayAssets, string location)
        {
            foreach(Asset asset in interplayAssets)
            {
                InterplayService.UpdateBackupStatus(asset.InterplayMobID);
            }
        }

        private void MoveToFolder(List<Asset> interplayAssets, string location)
        {
         //   return;
            // get assets from the database
            List<Asset> allDatabaseAssets = DatabaseIO.GetAssets(false, location);
            if (allDatabaseAssets == null)
            {
                return;
            }


            // Shorten the list by taking only assets where Itx's lastmodified is with the last x months.
            List<Asset> recentDatabaseAssets = allDatabaseAssets.Where(x => x.Assetdata.LastModified > DateTime.Now.AddDays(-30)).ToList();

            // List of database assets present in Interplay
            List<Asset> presentDatabaseAssets = recentDatabaseAssets.Intersect(interplayAssets, new IdComparer()).ToList();
            

            foreach (Asset asset in presentDatabaseAssets)
            {
                string YearSubfolder = GenericFunctions.GetYearSubfolder(asset);
                if (YearSubfolder != "") YearSubfolder = YearSubfolder + "/";
                string NewInterplayPath = $"/{_archiveFolder}/{GenericFunctions.GetDivision(asset)}/{GenericFunctions.GetYearFolder(asset)}/{YearSubfolder}";
                string CurrentInterplayFullPath = interplayAssets.First(x => x.AssetID == asset.AssetID).InterplayPath;
                string CurrentInterplayPath = CurrentInterplayFullPath.Remove(CurrentInterplayFullPath.LastIndexOf("/") + 1);
                if (CurrentInterplayPath != NewInterplayPath)
                {
                    if( InterplayService.MoveAsset(CurrentInterplayFullPath, NewInterplayPath) == true)
                    {
                        Logger.WriteConsoleAndLog(MsgType.INFO, null, $"Moved {asset.Assetdata.AssetName} to {NewInterplayPath}");
                    }
                }
            }
        }

        private void UpdateLocators(List<Asset> interplayAssets, string location)
        {
            // get assets from the database
            List<Asset> databaseAssets = DatabaseIO.GetAssets(false, location);
            if (databaseAssets == null)
            {
                return;
            }

            List<Asset> AssetWithLogging = databaseAssets.Where(x => x.Assetdata.Logevents.Length > 0).ToList();
            DateTime Afterdate = DateTime.Now.AddDays(-10);
            List<Asset> RecentAssetWithLogging = databaseAssets.Where(x => x.Assetdata.Logevents.Length > 0 && x.Assetdata.LastModified > Afterdate && x.IngestStatus != null && x.IngestStatus == "finished" ).ToList();

            foreach (Asset asset in RecentAssetWithLogging)
            {
                List<Locator> itxLocatorList = new List<Locator>();
                foreach (LoggingMetadata LoggingEvent in asset.Assetdata.Logevents)
                {
                    Locator locator = new Locator();
                    locator.TimeCode = LoggingEvent.Inpoint;
                    locator.Comment = LoggingEvent.Description.Trim().Replace("\r","");   // Interplay uses only \n in comments
                    locator.Color = "White";
                    locator.UserName = "iNOS";
                    locator.ITXAssetID = asset.AssetID;
                    locator.Track = "V1";
                    locator.FramensFromStart = LoggingEvent.FramesFromStart;
                    if(!string.IsNullOrWhiteSpace(locator.Comment))
                        itxLocatorList.Add(locator);
                }

                List<Locator> avidLocatorList = InterplayService.GetUmidLocators(asset.InterplayMobID);


                // update record with avidlocators, eventgough it might not be up-tp-date with this run.
                List<Locator> avidLocatorListOrdered = avidLocatorList.OrderBy(o => o.FramensFromStart).ToList();
                DatabaseIO.UpdateAsset( new Asset() { AssetID = asset.AssetID,  IngestLocation = location,  Assetdata = new AssetData() {  AvidLocators = avidLocatorListOrdered } } );

                // get locators from ITX with duplicate inpoints
                var anyITXDuplicate = itxLocatorList.GroupBy(x => x.FramensFromStart).Any(g => g.Count() > 1);
                if (anyITXDuplicate == true)
                {
                    Logger.WriteConsoleAndLog(MsgType.WARN, null, $"ITX Asset {asset.Assetdata.AssetName} has duplicate events");
                    continue;
                }

                // get locators from Avid with duplicate inpoints
                var anyAvidDuplicate = avidLocatorList.GroupBy(x => x.FramensFromStart).Any(g => g.Count() > 1);
                if (anyAvidDuplicate == true)
                {
                    Logger.WriteConsoleAndLog(MsgType.WARN, null, $"Avid Asset {asset.Assetdata.AssetName} has duplicate events");
                    continue;
                }




                // add new locators
                List<Locator> NewLocators = itxLocatorList.Where(itxlocator => !avidLocatorList.Any(avidlocator => avidlocator.FramensFromStart == itxlocator.FramensFromStart)).ToList();
                if(NewLocators.Count > 0)
                {
                    string returnval = InterplayService.SaveUmidLocator(NewLocators, asset.InterplayMobID);
                    if (returnval != "")
                    {
                        Logger.WriteConsoleAndLog(MsgType.ERROR, asset.Assetdata.AssetName, $"AddLocators: {returnval}");
                    }
                    else
                    {
                        Logger.WriteConsoleAndLog(MsgType.INFO, asset.Assetdata.AssetName, $"AddLocators: {asset.Assetdata.AssetName} {String.Join(" ", NewLocators.Select(x => x.TimeCode))}");
                    }
                }


                // Get list of where comment or color have changed
                // Add the corresponding locatorUri to each item of this list.
                List <Locator> ChangedLocators = itxLocatorList.Where(itxlocator => avidLocatorList.Any(avidlocator => avidlocator.FramensFromStart == itxlocator.FramensFromStart
                                                                                                             && (avidlocator.Comment != itxlocator.Comment
                                                                                                             || avidlocator.Color != itxlocator.Color)
                                                                                                             )).ToList();

                ChangedLocators.ForEach(x => x.LocatorUri = avidLocatorList.First(y => y.FramensFromStart == x.FramensFromStart).LocatorUri);
                if (ChangedLocators.Count > 0)
                {
                    string returnval = InterplayService.SaveUmidLocator(ChangedLocators, asset.InterplayMobID);
                    if (returnval != "")
                    {
                        Logger.WriteConsoleAndLog(MsgType.ERROR, asset.Assetdata.AssetName, $"ChangeLocators: {returnval}");
                    }
                    else
                    {
                        Logger.WriteConsoleAndLog(MsgType.INFO, asset.Assetdata.AssetName, $"ChangeLocators: {asset.Assetdata.AssetName} {String.Join(" ", ChangedLocators.Select(x=> x.TimeCode))}");
                    }
                }

                // remove obsolete locators where inpoint does not match.
                List<Locator> obsoleteLocators = avidLocatorList.Where(avidlocator => !itxLocatorList.Any(itxlocator => (itxlocator.FramensFromStart == avidlocator.FramensFromStart))).ToList();
                if(obsoleteLocators.Count > 0)
                {
                    string returnval = InterplayService.DeleteUmidLocators(obsoleteLocators, asset.InterplayMobID);
                    if (returnval != "")
                    {
                        Logger.WriteConsoleAndLog(MsgType.ERROR, asset.Assetdata.AssetName, $"Deletelocators: {returnval}");
                    }
                    else
                    {
                        Logger.WriteConsoleAndLog(MsgType.INFO, asset.Assetdata.AssetName, $"Deletelocators: {asset.Assetdata.AssetName} {String.Join(" ", ChangedLocators.Select(x => x.TimeCode))}");
                    }
                }
            }
        }




        private void FlagDeletedClips(List<Asset> interplayAssets, string location)
        {
            // ################################################

            // get deleted assets from the database
            List<Asset> deletedDatabaseAssets = DatabaseIO.GetAssets(true,location);

            // get all interplay assets where 'iNOS Housekeeping' <> "T-0"'
            List<Asset> interplayAssetsNotMarkedForDeletion = interplayAssets.Where(x => (x.AssetDeleted == null || false)).ToList();

            // Asset occuring in both lists need to be marked for deletion
            List<Asset> interplayClipsToMarkForDeletion = deletedDatabaseAssets.Where(x => interplayAssetsNotMarkedForDeletion.Any(y => x.AssetID == y.AssetID)).ToList();

            // Remove any clips with no mobid ( The clips where deleted before they where ingested to Interplay
            List<Asset> interplayClipsToMarkForDeletionNoMOb = interplayClipsToMarkForDeletion.Where(x => x.InterplayMobID != null).ToList();
            foreach (Asset asset in interplayClipsToMarkForDeletionNoMOb)
            {
                InterplayService.UpdateMultiAttributes(asset);
            }
        }

        private void UpdateAttributes(List<Asset> interplayAssets, string location)
        {
            // get assets from the database
            List<Asset> databaseAssets = DatabaseIO.GetAssets(false, location);
            if (databaseAssets == null)
            {
                return;
            }
            // List of database assets present in Interplay
            List<Asset> presentDatabaseAssets = databaseAssets.Intersect(interplayAssets, new IdComparer()).ToList();

            // Filter those Assets where name, title, contenttype, vervaldatum or verhaal datum have changed
            List<Asset> UpdateAssets = presentDatabaseAssets.Except(interplayAssets, new DifferenceComparer()).ToList();

            // update all
            foreach (Asset asset in UpdateAssets)
            {
                InterplayService.UpdateMultiAttributes(asset);
            }
        }



        private void CheckForAnomalies(List<Asset> interplayAssets, string location)
        {
            var DuplicateAssets = interplayAssets.GroupBy(x => x.Assetdata.AssetName)
              .Where(g => g.Count() > 1)
              .Select(y => y.Key)
              .ToList();

            foreach (var Asset in DuplicateAssets)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Asset: {Asset} is a duplicate");
            }
        }

        public class IdComparer : IEqualityComparer<Asset>
        {
            public int GetHashCode(Asset co)
            {
                if (co == null)
                {
                    return 0;
                }
                return co.AssetID.GetHashCode();
            }

            public bool Equals(Asset x1, Asset x2)
            {
                if (object.ReferenceEquals(x1, x2))
                {
                    return true;
                }
                if (object.ReferenceEquals(x1, null) ||
                    object.ReferenceEquals(x2, null))
                {
                    return false;
                }
                return x1.AssetID == x2.AssetID;
            }
        }

        public class DifferenceComparer : IEqualityComparer<Asset>
        {
            public int GetHashCode(Asset co)
            {
                if (co == null)
                {
                    return 0;
                }
                int hashAssetID = co.AssetID.GetHashCode();
                int hashContentType = co.Assetdata.ContentType.GetHashCode();

                return hashAssetID ^ hashContentType;

            //    return co.Assetdata.LastModified.GetHashCode();
            }

            public bool Equals(Asset x1, Asset x2)
            {
                if (object.ReferenceEquals(x1, x2))
                {
                    return true;
                }
                if (object.ReferenceEquals(x1, null) ||
                    object.ReferenceEquals(x2, null))
                {
                    return false;
                }
                return x1.Assetdata.ContentType == x2.Assetdata.ContentType
                                && x1.AssetID == x2.AssetID
                                && x1.Assetdata.AssetName == x2.Assetdata.AssetName
                                && x1.Assetdata.AssetTitle == x2.Assetdata.AssetTitle
                                && ( x1.Assetdata.ArchiveMetaDataSport?.VervalDatum?.ToString("yyyy-MM-dd") == x2.Assetdata.ArchiveMetaDataSport?.VervalDatum?.ToString("yyyy-MM-dd")
                                || x1.Assetdata.ArchiveMetadataNews?.VervalDatum?.ToString("yyyy-MM-dd") == x2.Assetdata.ArchiveMetadataNews?.VervalDatum?.ToString("yyyy-MM-dd"))
                                && x1.Assetdata.InosMetadata?.InosVerhaalDatum?.ToString("yyyy-MM-dd") == x2.Assetdata.InosMetadata?.InosVerhaalDatum?.ToString("yyyy-MM-dd");
            }
        }

    }
}
