﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedLib
{
    static public class GenericFunctions
    {

         static public string GetYearFolder(Asset asset)
        {
            if (asset.Assetdata.ContentType.Substring(0, 2) == "CS")
            {
                // Opnamedatum is preferred Yearfolder for Sport
                if (asset.Assetdata.ArchiveMetaDataSport?.OpnameDatum != null)
                {
                    return asset.Assetdata.ArchiveMetaDataSport.OpnameDatum?.ToString("yyyy");
                }
                else
                {
                    return asset.Assetdata.ProductionDate?.ToString("yyyy");
                }
            }
            else if (asset.Assetdata.ContentType.Substring(0, 2) == "CN" || asset.Assetdata.ContentType.Substring(0, 2) == "CU" || asset.Assetdata.ContentType.Substring(0, 2) == "CC")
            {
                if (asset.Assetdata.ProductionMetadataNews?.OpnameDatum != null)
                {
                    if (asset.Assetdata.ProductionMetadataNews.OpnameDatum?.ToString("yyyy-MM-dd HH:mm:ss") == "1970-01-01 12:00:00")
                    {
                        return asset.Assetdata.ProductionDate?.ToString("yyyy");
                    }
                    else
                    {
                        return asset.Assetdata.ProductionMetadataNews.OpnameDatum?.ToString("yyyy");
                    }
                }
                else
                {
                    return asset.Assetdata.ProductionDate?.ToString("yyyy");
                }
            }
            else
            {
                return asset.Assetdata.ProductionDate?.ToString("yyyy");
            }
        }

        static public string GetDivision(Asset asset)
        {
            string DivisionFolder = "RedactieZonderNaam";
            if (asset.Assetdata.ContentType.Substring(0, 2) == "CN")
            {
                DivisionFolder = "Nieuws";
            }
            if (asset.Assetdata.ContentType.Substring(0, 2) == "CY")
            {
                DivisionFolder = "Nieuws";
            }
            if (asset.Assetdata.ContentType.Substring(0, 2) == "CH")
            {
                DivisionFolder = "Nieuws";
            }
            else if (asset.Assetdata.ContentType.Substring(0, 2) == "CU")
            {
                DivisionFolder = "Nieuwsuur";
            }
            else if (asset.Assetdata.ContentType.Substring(0, 2) == "CS")
            {
                DivisionFolder = "Sport";
            }
            else if (asset.Assetdata.ContentType.Substring(0, 2) == "CC")
            {
                DivisionFolder = "Evenementen";
            }
            return DivisionFolder;
        }

        static public string GetYearSubfolder(Asset asset)
        {
            string DivisionSubFolder = "???";
            if (asset.Assetdata.ContentType.Substring(2, 1) == "A")
            {
                DivisionSubFolder = "";
            }
            else if (asset.Assetdata.ContentType.Substring(2, 1) == "D")
            {
                DivisionSubFolder = "";
            }
            else if (asset.Assetdata.ContentType.Substring(2, 1) == "K")
            {
                DivisionSubFolder = "";
            }
            else if (asset.Assetdata.ContentType.Substring(2, 1) == "I")
            {
                DivisionSubFolder = "Ingest ITX";
            }
            else if (asset.Assetdata.ContentType.Substring(2, 1) == "G")
            {
                DivisionSubFolder = "Schone Inlas ITX";
            }
            else if (asset.Assetdata.ContentType.Substring(2, 1) == "P")
            {
                DivisionSubFolder = "Playout ITX";
            }
            else if (asset.Assetdata.ContentType.Substring(2, 1) == "V")
            {
                DivisionSubFolder = "Vormgeving ITX";
            }
            else if (asset.Assetdata.ContentType.Substring(2, 1) == "T")
            {
                DivisionSubFolder = "Ingest ITX";
            }
            return DivisionSubFolder;
        }
    }
}
