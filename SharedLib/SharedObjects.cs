﻿using Newtonsoft.Json.Converters;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SharedLib
{
    public class Asset
    {
        public long AssetID { get; set; }
        public ITXLocation? ITXLocation { get; set; }
        public string FileUMID { get; set; }
        public string FileDecodeSucces { get; set; }
        public string ErrorDescription { get; set; }
        public bool? AssetDeleted { get; set; }
        public string IngestStatus { get; set; }
        public string IngestLocation { get; set; }
        public DateTime? IngestStarted { get; set; }
        public string IngestMessage { get; set; }
        public int? IngestProgress { get; set; }
        public string IngestErrorMessage { get; set; }
        public string InterplayMobID { get; set; }
        public string InterplayPath { get; set; }
        public long? InterplaysizeBytes { get; set; }
        public MediaInfo FileMediaInfo { get; set; }
        public AssetData Assetdata { get; set; }
    }


    public class AssetData
    {
        public long AssetID { get; set; }
        public string AssetName { get; set; }
        public string Guid { get; set; }
        public string AssetTitle { get; set; }
        public string ContentType { get; set; }
        public string Inpoint { get; set; }
        public string MediaLocation { get; set; }
        public InosMetadata InosMetadata { get; set; }
        public ProductionMetataNews ProductionMetadataNews { get; set; }
        public ArchiveMetaDataNews ArchiveMetadataNews { get; set; }
        public ProductionMetadataSport ProductionMetadataSport { get; set; }
        public ArchiveMetaDataSport ArchiveMetaDataSport { get; set; }
        public LoggingMetadata[] Logevents { get; set; }
        public string[] Categories { get; set; }
        public DateTime? ProductionDate { get; set; }
        public DateTime? LastModified { get; set; }
        public DateTime? Created { get; set; }
        public string Duration { get; set; }
        public string Notes { get; set; }
        public bool? IsPlaceholder { get; set; }
        public List<Locator> AvidLocators { get; set; }
    }


    public class InosMetadata
    {
        public string gewassen { get; set; }
        public string VerhaalNaam { get; set; }
        public int? VerhaalID { get; set; }
        public string ItemNaam { get; set; }
        public int? ItemID { get; set; }
        public int? TaakID { get; set; }
        public int? InosAssetID { get; set; }
        public DateTime? InosVerhaalDatum { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public InosVerhaalType VerhaalType { get; set; }
        public string Bron { get; set; }
    }

    public class ArchiveMetaDataSport
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public RechtenVrij_EN RechtenVrij { get; set; }
        public string Onderwerp { get; set; }
        public string Locatie { get; set; }
        public DateTime? OpnameDatum { get; set; }
        public string Rechten { get; set; }
        public string Verslaggever { get; set; }
        public string Tape { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Geluid Geluid { get; set; }
        public string[] Trefwoorden { get; set; }
        public DateTime? VervalDatum { get; set; }
    }




    public class ArchiveMetaDataNews
    {
        public string Archivaris { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ArchiefStatus Status { get; set; }
        public string[] Trefwoorden { get; set; }
        public string[] Personen { get; set; }
        public string[] Spreker { get; set; }
        public string Vegetatie { get; set; }
        public string Weer { get; set; }
        public string Beschrijving { get; set; }
        public DateTime? VervalDatum { get; set; }

    }

    public class ProductionMetadataSport
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public RechtenVrij_EN RechtenVrij { get; set; }
        public string Rechten { get; set; }
        public string Sport { get; set; }
        public string Evenement { get; set; }
        public string Locatie { get; set; }
        public DateTime? OpnameDatum { get; set; }
        public string Verslaggever { get; set; }
        public string[] Redacteuren { get; set; }
        public string Wisbeleid { get; set; }
        public string Notities { get; set; }
    }
    public class ProductionMetataNews
    {
        public string Redactie { get; set; }
        public string[] Locatie { get; set; }
        public string[] Verslaggever { get; set; }
        public string CameraPloeg { get; set; }
        public DateTime? OpnameDatum { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public RechtenVrij_EN RechtenVrij { get; set; }
        public string Rechten { get; set; }
        public string VerhaalNaam { get; set; }
        public long? VerhaalID { get; set; }
        public string[] Bron { get; set; }
        public string Wisbeleid { get; set; }
        public string Memo { get; set; }
    }


    public class LoggingMetadata
    {
        public long ITXMetadataID { get; set; }
        public string RangeWarning { get; set; }
        public bool IsPast24hr { get; set; }
        public long FramesFromStart { get; set; }
        public long Duration2 { get; set; }
        public string Inpoint { get; set; }
        public string Duration { get; set; }
        public string Description { get; set; }
    }

    public enum ITXLocation
    {
        hlv,
        dhg
    }
    public enum ArchiefStatus
    {
        Klaar,
        NietGereed,
        Sport
    }

    public enum InosVerhaalType
    {
        Verhaal,
        Plank,
        Obit,
        Project
    }

    public enum Geluid
    {
        Algemeen,
        Gescheiden,
        Commentaar
    }

    public enum RechtenVrij_EN
    {
        Green,
        Amber,
        Red
    }

    public enum RechtenVrij_NL
    {
        Groen,
        Oranje,
        Rood
    }



    public class MediaInfo
    {
        public string general_Format { get; set; }
        public string general_AudioTracks { get; set; }
        public string general_VideoTracks { get; set; }
        public string general_Format_Profile { get; set; }
        public string general_Format_Settings { get; set; }
        public string general_Duration { get; set; }
        public string general_FrameRate { get; set; }
        public string general_FileSize { get; set; }
        public string video_Format { get; set; }
        public string video_Format_Profile { get; set; }
        public string video_Format_Level { get; set; }
        public string video_Format_Settings_Wrapping { get; set; }
        public string video_BitRate { get; set; }
        public string video_Width { get; set; }
        public string video_Height { get; set; }
        public string video_BitDepth { get; set; }
        public string video_ScanType { get; set; }
        public string audio_Format { get; set; }
        public string audio_Channels { get; set; }
        public string audio_SamplingRate { get; set; }
        public string audio_BitDepth { get; set; }
        public string other_Timecode { get; set; }
    }

    public class Locator
    {
        public string UserName { get; set; }
        public string TimeCode { get; set; }
        public string Color { get; set; }
        public string Track { get; set; }
        public string Comment { get; set; }
        public long ITXAssetID { get; set; }
        public string LocatorUri { get; set; }
        public long FramensFromStart { get; set; }
    }

    public class SearchFilter
    {
        public string Name { get; set; }
        public string Group { get; set; }
        public string Value { get; set; }
        public string Condition { get; set; }
    }
    public class SharedObjects
    {
    }

}