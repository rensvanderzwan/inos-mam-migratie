﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Reflection;
using System.Reflection.Metadata;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace SharedLib
{
    public class CustomInterplayAttribute
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class MediaDirector
    {
        public enum TrancodeFormats
        {
            [Description("transparent pass-through")] transparent_Pass_Through,
            [Description("XDCAM-EX 35")] XDcam_EX_35,
            [Description("XDCAM-HD 50")] XDcam_HD_50,
            [Description("IMX 30")] Imx30,
            [Description("IMX 50")] Imx50,
            [Description("H.264 800k Class (with PCM)")] H264_800k_Class_With_PCM,
            [Description("XAVC-Intra 4:2:2")] XAVC_Intra_422,
            [Description("AVC-Intra 100")] AVC_Intra_100,
            [Description("DNxHD High Quality (10 bit)")] DNxHD_High_Quality_10_bit,


            //public const string XDcam_HD_50 = "XDCAM-HD 50";
            //public const string Imx30 = "IMX 30";
            //public const string Imx50 = "IMX 50";
            //public const string H264_800k_Class_With_PCM = "H.264 800k Class (with PCM)";
            //public const string XAVC_Intra_422 = "XAVC-Intra 4:2:2";
            //public const string AVC_Intra_100 = "AVC-Intra 100";
            //public const string DNxHD_High_Quality_10_bit = "DNxHD High Quality (10 bit)";
        }

        private  NameValueCollection _appSettings;
        public string _mdUrl;
        public string _mdUser;
        public string _mdPass;
        public string _api = "/api/md";
        private string _profileTemplateUri;
        private string _profileUri;
        private string _processesUri;
        public string _ingestLocation; // choice between MD Hilversum of Den Haag.
        private List<string> _profileIDs = new List<string>();

        private WebClient _client = new WebClient();
        private Dictionary<string, string> _profileTempates = new Dictionary<string, string>();        // store all profile templates
        public List<string> progressUris = new List<string>();

 
        public MediaDirector(string Host,string Login, string Paswd)
        {
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            _ingestLocation = _appSettings["ingest_location"];
            _mdUrl = Host;
            _mdUser = Login;
            _mdPass = Paswd;

            _profileTemplateUri = _mdUrl + _api + "/profiletemplates";
            _profileUri = _mdUrl + _api + "/profiles";
            _processesUri = _mdUrl + _api + "/processes";
        }

        public void getProfileTemplate(string uri)
        {
            try
            {
                var URI = new Uri(uri);
                _client.Headers["Accept"] = "application/vnd.avid.md.profile+xml;version=4.0";
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(_mdUser + ":" + _mdPass));
                _client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                string response = _client.DownloadString(URI);
                _profileTempates.Add(Path.GetFileName(URI.LocalPath), response);
                processProfileTemplate(response);
            }
            catch (WebException e)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "Status:" + e.Status + " Message:" + e.Message);
            }
        }


        public void getProfile(string uri)
        {
            try
            {
                var URI = new Uri(uri);
                _client.Headers["Accept"] = "application/vnd.avid.md.profile+xml;version=4.0";
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(_mdUser + ":" + _mdPass));
                _client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                string response = _client.DownloadString(URI);
                processProfile(response);
            }
            catch (WebException e)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "Status:" + e.Status + " Message:" + e.Message);
            }
        }

        public void processProfileTemplate(string xml)
        {

        }

        public void processProfile(string uri)
        {

        }

        public void getProfileTemplates()
        {
            try
            {
                string URIstr = _profileTemplateUri;
                var URI = new Uri(URIstr);
                _client.Headers["Accept"] = "application/vnd.avid.md.profiles+xml;version=4.0";
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(_mdUser + ":" + _mdPass));
                _client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                string response = _client.DownloadString(URI);
                processProfileTemplates(response);
            }
            catch (WebException e)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "Status:" + e.Status + " Message:" + e.Message);
            }
        }

        public void getProfiles()
        {
            try
            {
                string URIstr = _profileUri;
                var URI = new Uri(URIstr);
                _client.Headers["Accept"] = "application/vnd.avid.md.profiles+xml;version=4.0";
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(_mdUser + ":" + _mdPass));
                _client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                string response = _client.DownloadString(URI);
                processProfiles(response);
            }
            catch (WebException e)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "Status:" + e.Status + " Message:" + e.Message);
            }
        }

        private void processProfiles(string response)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response);
            XmlElement root = doc.DocumentElement;


            XmlNodeList nodeList = doc.SelectNodes("//profile");
            foreach (XmlNode node in nodeList)
            {
                _profileIDs.Add(node.Attributes["href"].Value);
                getProfile(node.Attributes["href"].Value);
            }

        }

        private void processProfileTemplates(string response)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response);
            XmlElement root = doc.DocumentElement;


            XmlNodeList nodeList = doc.SelectNodes("//profile");
            foreach (XmlNode node in nodeList)
            {
                getProfileTemplate(node.Attributes["href"].Value);
            }
        }

        public string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }

        public void ingestDirectIngestViaCache(string sourceUri, string clipName, string interplayPath, string NexisWorkspaceUri, string videoID, string sourceProvider, TrancodeFormats tf = TrancodeFormats.transparent_Pass_Through)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"ProfileTemplates\", "DirectViaCache.xml");
            string template = File.ReadAllText(path);
            string xml = buildDirectIngestViaCacheXML(template, sourceUri, clipName, interplayPath, NexisWorkspaceUri, sourceProvider, GetEnumDescription(tf));

            if (!ValidateSchema(xml, $"{_mdUrl}/profileschema/Process_ParkCacheRegisterTranscode.xsd", out string error))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "XML for clip " + clipName + " is not valid error message: " + error);
                return;
            }

            if (!postProcess(xml, out string result))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "postProcess returned an error for clip " + clipName + " error message: " + error);
                return;
            }

            progressUris.Add(result);
        }



        public void ingestDirectIngest(string sourceUri, string clipName, string interplayPath, string NexisWorkspaceUri, string videoID = "", TrancodeFormats tf = TrancodeFormats.transparent_Pass_Through)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"ProfileTemplates\", "DirectIngest.xml");
            string template = File.ReadAllText(path);
            string xml = buildDirectIngestXML(template, sourceUri, clipName, interplayPath, NexisWorkspaceUri, "sourceProviderPool1", GetEnumDescription(tf));

            if (!ValidateSchema(xml, $"{_mdUrl}/profileschema/Process_RegisterTranscode.xsd", out string error))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "XML for clip " + clipName + " is not valid error message: " + error);
                return;
            }

            if (!postProcess(xml, out string result))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "postProcess returned an error for clip " + clipName + " error message: " + error);
                return;
            }

            progressUris.Add(result);
        }

        public string ingestFromOp1a(string sourceUri, string clipName, string interplayPath, string NexisWorkspaceUri, List<CustomInterplayAttribute> CustomAttributes = null)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"ProfileTemplates\", "IngestFromOp1a.xml");
            string template = File.ReadAllText(path);
            string xml = buildOp1aIngestXML(template, sourceUri, clipName, interplayPath, NexisWorkspaceUri, "sourceProviderPool1", CustomAttributes);

            if (!ValidateSchema(xml, $"{_mdUrl}/profileschema/2.0/Process_IngestFromOp1a.xsd", out string error))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "XML for clip " + clipName + " is not valid error message: " + error);
                return null;
            }

            if (!postProcess(xml, out string result))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "postProcess returned an error for clip " + clipName + " error message: " + error);
                return null;
            }
            return result;
           // progressUris.Add(result);
        }

        public void outgestToOp1a(string mobID, string clipName, string interplayPath, string outPath, string mixdownWorkspace, TrancodeFormats tf = TrancodeFormats.transparent_Pass_Through)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"ProfileTemplates\", "OutgestToOP1a.xml");
            string template = File.ReadAllText(path);
            string xml = buildOp1aOutgestXML(template, mobID, clipName, interplayPath, outPath, mixdownWorkspace, GetEnumDescription(tf));

            if (!ValidateSchema(xml, $"{_mdUrl}/profileschema/2.0/Process_OutgestToOp1a.xsd", out string error))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "XML for clip " + clipName + " is not valid error message: " + error);
                return;
            }

            if (!postProcess(xml, out string result))
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "postProcess returned an error for clip " + clipName + " error message: " + error);
                return;
            }

            progressUris.Add(result);
        }

        private string buildOp1aOutgestXML(string profile, string mobID, string Clipname, string IPPathName, string outPath, string mixdownWorkspace, string transcode)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(profile);
            XmlNode root = doc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("bk", root.NamespaceURI);

            // Vervang template lokatie met nieuwe lokatie
            XmlNode OldPath = root.SelectSingleNode("descendant::bk:sourceVolumeURI", nsmgr);
            XmlElement NewPath = doc.CreateElement("sourceVolumeURI", root.NamespaceURI);
            NewPath.InnerText = mobID;
            OldPath.ParentNode.ReplaceChild(NewPath, OldPath);


            // Vervang destination Path met nieuw Path
            XmlNodeList OldWorkspace1 = root.SelectNodes("descendant::bk:outputFilesystemVolumeURIBuildingRule", nsmgr);
            foreach (XmlNode Node in OldWorkspace1)
            {
                XmlElement NewWorkspaceX = doc.CreateElement("outputFilesystemVolumeURIBuildingRule", root.NamespaceURI);
                NewWorkspaceX.InnerText = outPath;
                Node.ParentNode.ReplaceChild(NewWorkspaceX, Node);
            }

            // Vervang destination Path met nieuw Path
            XmlNodeList OldWorkspace2 = root.SelectNodes("descendant::bk:outputFilesystemVolumeMetaURIBuildingRule", nsmgr);
            foreach (XmlNode Node in OldWorkspace2)
            {
                XmlElement NewWorkspaceX = doc.CreateElement("outputFilesystemVolumeMetaURIBuildingRule", root.NamespaceURI);
                NewWorkspaceX.InnerText = outPath;
                Node.ParentNode.ReplaceChild(NewWorkspaceX, Node);
            }

            // Vervang template lokatie met nieuwe lokatie
            XmlNode oldOutputFilesystemBaseFileNameBuildingRule = root.SelectSingleNode("//bk:mediaTransformationDetail/bk:outputFilesystemBaseFileNameBuildingRule", nsmgr);
            XmlElement NewOtputFilesystemBaseFileNameBuildingRule = doc.CreateElement("outputFilesystemBaseFileNameBuildingRule", root.NamespaceURI);
            NewOtputFilesystemBaseFileNameBuildingRule.InnerText = Clipname;
            oldOutputFilesystemBaseFileNameBuildingRule.ParentNode.ReplaceChild(NewOtputFilesystemBaseFileNameBuildingRule, oldOutputFilesystemBaseFileNameBuildingRule);

            // Vervang outputFormatLabel met nieuw label
            XmlNode OldoutputFormatLabel = root.SelectSingleNode("//bk:mediaTransformationDetail/bk:outputFormatLabel", nsmgr);
            XmlElement NewoutputFormatLabel = doc.CreateElement("outputFormatLabel", root.NamespaceURI);
            NewoutputFormatLabel.InnerText = transcode;
            OldoutputFormatLabel.ParentNode.ReplaceChild(NewoutputFormatLabel, OldoutputFormatLabel);

            // Vervang isisMixdownWorkspace met nieuwe
            XmlNode oldIsisMixdownWorkspace = root.SelectSingleNode("//bk:mediaTransformationDetail/bk:isisMixdownWorkspace", nsmgr);
            XmlElement newIsisMixdownWorkspace = doc.CreateElement("isisMixdownWorkspace", root.NamespaceURI);
            newIsisMixdownWorkspace.InnerText = mixdownWorkspace;
            oldIsisMixdownWorkspace.ParentNode.ReplaceChild(newIsisMixdownWorkspace, oldIsisMixdownWorkspace);

            return doc.OuterXml;
        }



        private string buildOp1aIngestXML(string profile, string Path, string Clipname, string IPPathName, string Workspace, string ProviderName, List<CustomInterplayAttribute> CustomAttributes)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(profile);
            XmlNode root = doc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("bk", root.NamespaceURI);

            // Vervang template lokatie met nieuwe lokatie
            XmlNode OldPath = root.SelectSingleNode("descendant::bk:sourceVolumeURI", nsmgr);
            XmlElement NewPath = doc.CreateElement("sourceVolumeURI", root.NamespaceURI);
            NewPath.InnerText = Path;
            OldPath.ParentNode.ReplaceChild(NewPath, OldPath);

            // Vervang template Interplaypath met nieuw pad
            XmlNode OldFolder = root.SelectSingleNode("descendant::bk:destinationProductionVolumeURIBuildingRule", nsmgr);
            XmlElement NewFolder = doc.CreateElement("destinationProductionVolumeURIBuildingRule", root.NamespaceURI);
            NewFolder.InnerText = IPPathName;
            OldFolder.ParentNode.ReplaceChild(NewFolder, OldFolder);

            // Vervang template clipnaam met nieuwe clipnaam
            XmlNode OldClip = root.SelectSingleNode("descendant::bk:destinationProductionClipNameBuildingRule", nsmgr);
            XmlElement NewClip = doc.CreateElement("destinationProductionClipNameBuildingRule", root.NamespaceURI);
            NewClip.InnerText = Clipname;
            OldClip.ParentNode.ReplaceChild(NewClip, OldClip);

            // Vervang source provider door de MediaDirector(Hier draait de CopyProvider op)
            XmlNode OldProvider = root.SelectSingleNode("descendant::bk:sourceProviderPool", nsmgr);
            XmlElement NewProvider = doc.CreateElement("sourceProviderPool", root.NamespaceURI);
            NewProvider.InnerText = ProviderName;
            OldProvider.ParentNode.ReplaceChild(NewProvider, OldProvider);


            XmlNode createSequenceForRegisteredClips = root.SelectSingleNode("//bk:metadataPamRepresentationDetailList/bk:registerAmaMedia", nsmgr);
            XmlElement attributeList = doc.CreateElement("attributeList", root.NamespaceURI);
            foreach (CustomInterplayAttribute Attribute in CustomAttributes)
            {
                XmlElement attribute = doc.CreateElement("attribute", root.NamespaceURI);
                XmlElement name = doc.CreateElement("name", root.NamespaceURI);
                name.InnerText = Attribute.name;
                XmlElement value = doc.CreateElement("value", root.NamespaceURI);
                value.InnerText = Attribute.value;

                attribute.AppendChild(name);
                attribute.AppendChild(value);
                attributeList.AppendChild(attribute);
            }

            createSequenceForRegisteredClips.ParentNode.InsertAfter(attributeList, createSequenceForRegisteredClips);

            // Vervang ISIS Workspace met nieuwe Workspace
            XmlNodeList OldWorkspace2 = root.SelectNodes("descendant::bk:outputFilesystemVolumeURIBuildingRule", nsmgr);
            foreach (XmlNode Node in OldWorkspace2)
            {
                XmlElement NewWorkspaceX = doc.CreateElement("outputFilesystemVolumeURIBuildingRule", root.NamespaceURI);
                NewWorkspaceX.InnerText = Workspace;
                Node.ParentNode.ReplaceChild(NewWorkspaceX, Node);
            }
            return doc.OuterXml;
        }

        private string buildDirectIngestViaCacheXML(string profile, string Path, string Clipname, string IPPathName, string Workspace, string ProviderName, string outputFormat)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(profile);
            XmlNode root = doc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("bk", root.NamespaceURI);

            // Vervang template lokatie met nieuwe lokatie
            XmlNode OldPath = root.SelectSingleNode("descendant::bk:sourceVolumeURI", nsmgr);
            XmlElement NewPath = doc.CreateElement("sourceVolumeURI", root.NamespaceURI);
            NewPath.InnerText = Path;
            OldPath.ParentNode.ReplaceChild(NewPath, OldPath);

            // Vervang template Interplaypath met nieuw pad
            XmlNode OldDestinationFilesystemVolumeURIBuildingRule = root.SelectSingleNode("descendant::bk:destinationFilesystemVolumeURIBuildingRule", nsmgr);
            XmlElement NewDestinationFilesystemVolumeURIBuildingRule = doc.CreateElement("destinationFilesystemVolumeURIBuildingRule", root.NamespaceURI);
            NewDestinationFilesystemVolumeURIBuildingRule.InnerText = @"\\ofamd001\MediaAssetManager\";
            OldDestinationFilesystemVolumeURIBuildingRule.ParentNode.ReplaceChild(NewDestinationFilesystemVolumeURIBuildingRule, OldDestinationFilesystemVolumeURIBuildingRule);

            // Vervang template Interplaypath met nieuw pad
            XmlNode OldFolder = root.SelectSingleNode("descendant::bk:destinationProductionVolumeURIBuildingRule", nsmgr);
            XmlElement NewFolder = doc.CreateElement("destinationProductionVolumeURIBuildingRule", root.NamespaceURI);
            NewFolder.InnerText = IPPathName;
            OldFolder.ParentNode.ReplaceChild(NewFolder, OldFolder);

            // Vervang template clipnaam met nieuwe clipnaam
            XmlNode OldClip = root.SelectSingleNode("descendant::bk:destinationProductionClipNameBuildingRule", nsmgr);
            XmlElement NewClip = doc.CreateElement("destinationProductionClipNameBuildingRule", root.NamespaceURI);
            NewClip.InnerText = Clipname;
            OldClip.ParentNode.ReplaceChild(NewClip, OldClip);

            // Vervang source provider door de MediaDirector(Hier draait de CopyProvider op)
            XmlNode OldProvider = root.SelectSingleNode("descendant::bk:sourceProviderPool", nsmgr);
            XmlElement NewProvider = doc.CreateElement("sourceProviderPool", root.NamespaceURI);
            NewProvider.InnerText = ProviderName;
            OldProvider.ParentNode.ReplaceChild(NewProvider, OldProvider);

            // Voeg Video ID attribute toe
            XmlNode createSequenceForRegisteredClips = root.SelectSingleNode("//bk:metadataPamRepresentationDetailList/bk:createSequenceForRegisteredClips", nsmgr);
            XmlElement attributeList = doc.CreateElement("attributeList", root.NamespaceURI);
            XmlElement attribute = doc.CreateElement("attribute", root.NamespaceURI);
            XmlElement name = doc.CreateElement("name", root.NamespaceURI);
            name.InnerText = "Video ID";
            XmlElement value = doc.CreateElement("value", root.NamespaceURI);
            value.InnerText = Clipname;
            attribute.AppendChild(name);
            attribute.AppendChild(value);
            attributeList.AppendChild(attribute);
            createSequenceForRegisteredClips.ParentNode.InsertAfter(attributeList, createSequenceForRegisteredClips);

            // Vervang outputFormatLabel met nieuw label
            XmlNode OldoutputFormatLabel = root.SelectSingleNode("//bk:mediaTransformationDetail/bk:outputFormatLabel", nsmgr);
            XmlElement NewoutputFormatLabel = doc.CreateElement("outputFormatLabel", root.NamespaceURI);
            NewoutputFormatLabel.InnerText = outputFormat;
            OldoutputFormatLabel.ParentNode.ReplaceChild(NewoutputFormatLabel, OldoutputFormatLabel);

            // Vervang ISIS Workspace met nieuwe Workspace
            XmlNodeList OldWorkspace2 = root.SelectNodes("descendant::bk:outputFilesystemVolumeURIBuildingRule", nsmgr);
            foreach (XmlNode Node in OldWorkspace2)
            {
                XmlElement NewWorkspaceX = doc.CreateElement("outputFilesystemVolumeURIBuildingRule", root.NamespaceURI);
                NewWorkspaceX.InnerText = Workspace;
                Node.ParentNode.ReplaceChild(NewWorkspaceX, Node);
            }

            // Vervang createMixdownForTranscodedClips met nieuwe
            XmlNode OldcreateMixdownForTranscodedClips = root.SelectSingleNode("//bk:mediaTransformationDetailList/bk:createMixdownForTranscodedClips", nsmgr);
            XmlElement NewcreateMixdownForTranscodedClips = doc.CreateElement("createMixdownForTranscodedClips", root.NamespaceURI);
            NewcreateMixdownForTranscodedClips.InnerText = "false";
            OldcreateMixdownForTranscodedClips.ParentNode.ReplaceChild(NewcreateMixdownForTranscodedClips, OldcreateMixdownForTranscodedClips);


            return doc.OuterXml;
        }


        private string buildDirectIngestXML(string profile, string Path, string Clipname, string IPPathName, string Workspace, string ProviderName, string outputFormat)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(profile);
            XmlNode root = doc.DocumentElement;
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("bk", root.NamespaceURI);

            // Vervang template lokatie met nieuwe lokatie
            XmlNode OldPath = root.SelectSingleNode("descendant::bk:sourceVolumeURI", nsmgr);
            XmlElement NewPath = doc.CreateElement("sourceVolumeURI", root.NamespaceURI);
            NewPath.InnerText = Path;
            OldPath.ParentNode.ReplaceChild(NewPath, OldPath);

            // Vervang template Interplaypath met nieuw pad
            XmlNode OldFolder = root.SelectSingleNode("descendant::bk:destinationProductionVolumeURIBuildingRule", nsmgr);
            XmlElement NewFolder = doc.CreateElement("destinationProductionVolumeURIBuildingRule", root.NamespaceURI);
            NewFolder.InnerText = IPPathName;
            OldFolder.ParentNode.ReplaceChild(NewFolder, OldFolder);

            // Vervang template clipnaam met nieuwe clipnaam
            XmlNode OldClip = root.SelectSingleNode("descendant::bk:destinationProductionClipNameBuildingRule", nsmgr);
            XmlElement NewClip = doc.CreateElement("destinationProductionClipNameBuildingRule", root.NamespaceURI);
            NewClip.InnerText = Clipname;
            OldClip.ParentNode.ReplaceChild(NewClip, OldClip);

            // Vervang source provider door de MediaDirector(Hier draait de CopyProvider op)
            XmlNode OldProvider = root.SelectSingleNode("descendant::bk:sourceProviderPool", nsmgr);
            XmlElement NewProvider = doc.CreateElement("sourceProviderPool", root.NamespaceURI);
            NewProvider.InnerText = ProviderName;
            OldProvider.ParentNode.ReplaceChild(NewProvider, OldProvider);

            // Voeg Video ID attribute toe
            XmlNode createSequenceForRegisteredClips = root.SelectSingleNode("//bk:metadataPamRepresentationDetailList/bk:createSequenceForRegisteredClips", nsmgr);
            XmlElement attributeList = doc.CreateElement("attributeList", root.NamespaceURI);
            XmlElement attribute = doc.CreateElement("attribute", root.NamespaceURI);
            XmlElement name = doc.CreateElement("name", root.NamespaceURI);
            name.InnerText = "Video ID";
            XmlElement value = doc.CreateElement("value", root.NamespaceURI);
            value.InnerText = Clipname;
            attribute.AppendChild(name);
            attribute.AppendChild(value);
            attributeList.AppendChild(attribute);
            createSequenceForRegisteredClips.ParentNode.InsertAfter(attributeList, createSequenceForRegisteredClips);

            // Vervang outputFormatLabel met nieuw label
            XmlNode OldoutputFormatLabel = root.SelectSingleNode("//bk:mediaTransformationDetail/bk:outputFormatLabel", nsmgr);
            XmlElement NewoutputFormatLabel = doc.CreateElement("outputFormatLabel", root.NamespaceURI);
            NewoutputFormatLabel.InnerText = outputFormat;
            OldoutputFormatLabel.ParentNode.ReplaceChild(NewoutputFormatLabel, OldoutputFormatLabel);

            // Vervang ISIS Workspace met nieuwe Workspace
            XmlNodeList OldWorkspace2 = root.SelectNodes("descendant::bk:outputFilesystemVolumeURIBuildingRule", nsmgr);
            foreach (XmlNode Node in OldWorkspace2)
            {
                XmlElement NewWorkspaceX = doc.CreateElement("outputFilesystemVolumeURIBuildingRule", root.NamespaceURI);
                NewWorkspaceX.InnerText = Workspace;
                Node.ParentNode.ReplaceChild(NewWorkspaceX, Node);
            }

            // Vervang createMixdownForTranscodedClips met nieuwe
            XmlNode OldcreateMixdownForTranscodedClips = root.SelectSingleNode("//bk:mediaTransformationDetailList/bk:createMixdownForTranscodedClips", nsmgr);
            XmlElement NewcreateMixdownForTranscodedClips = doc.CreateElement("createMixdownForTranscodedClips", root.NamespaceURI);
            NewcreateMixdownForTranscodedClips.InnerText = "false";
            OldcreateMixdownForTranscodedClips.ParentNode.ReplaceChild(NewcreateMixdownForTranscodedClips, OldcreateMixdownForTranscodedClips);


            return doc.OuterXml;
        }


        private bool postProcess(string xml, out string result)
        {
            XmlDocument ProcessXML = new XmlDocument();
            XmlDeclaration xmlDeclaration = ProcessXML.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = ProcessXML.DocumentElement;
            ProcessXML.InsertBefore(xmlDeclaration, root);

            XmlElement process = ProcessXML.CreateElement("process");
            ProcessXML.AppendChild(process);
            XmlElement profile = ProcessXML.CreateElement("profile");
            process.AppendChild(profile);

            XmlCDataSection CData;
            CData = ProcessXML.CreateCDataSection(xml);
            profile.AppendChild(CData);

            try
            {
                string URIstr = _processesUri;
                var URI = new Uri(URIstr);
                WebClient client = new WebClient();
                client.Headers[HttpRequestHeader.ContentType] = "application/vnd.avid.md.process+xml";
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(_mdUser + ":" + _mdPass));
                client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                string HtmlResult = client.UploadString(URI, ProcessXML.OuterXml);
                WebHeaderCollection myWebHeaderCollection = client.ResponseHeaders;
                result = myWebHeaderCollection.Get("Location");
                return true;
            }
            catch (WebException e)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "Status:" + e.Status + " Message:" + e.Message);
                result = "Status:" + e.Status + " Message:" + e.Message;
                return false;
            }
        }





        public bool getProcess(string uri, out string response)
        {
            try
            {
                var URI = new Uri(uri);
                _client.Headers["Accept"] = "application/vnd.avid.md.process+xml";
                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(_mdUser + ":" + _mdPass));
                _client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                response = _client.DownloadString(URI);
                return true;
            }
            catch (WebException e)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, "Status:" + e.Status + " Message:" + e.Message);
                response = "Status:" + e.Status + " Message:" + e.Message;
                return false;
            }
        }

        public bool ValidateSchema(string xmlPath, string xsdPath, out string err)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlPath);

            xml.Schemas.Add(null, xsdPath);

            try
            {
                xml.Validate(null);
            }
            catch (XmlSchemaValidationException e)
            {
                err = e.Message;
                return false;
            }
            err = "Xml is valid";
            return true;
        }
    }
    }
