﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using SharedLib;

namespace RunMediaInfoFFProbe
{
    class Main
    {

        public void Start(string arg)
        {
            Logger._LogPath = $@"C:\Temp\DoMediaInfo_{arg}.log";
            while(true)
            {
                Asset asset = DatabaseIO.GetAssetToInfo();
                if(asset != null)
                {
                    Asset InfoAsset = Domediainfo(asset);
                    if ( InfoAsset.FileMediaInfo == null)
                    {
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Error getting mediainfo for {asset.AssetID}");
                    }
                    DatabaseIO.UpdateAsset(InfoAsset);
                }
                else
                {
                    break;
                }
            }
        }

        public Asset Domediainfo(Asset ITXAsset)
        {

            ShellProcess MIProcess = new ShellProcess("Utilities\\MediaInfo.exe", "--Output=XML " + $"\"{ITXAsset.Assetdata.MediaLocation}\"", 10, ConsoleCtrlEvent.CTRL_C, true);
            if (MIProcess.Process.ExitCode != 0)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"MediaInfo error: {MIProcess.Process.ExitCode} {MIProcess.Output.ToString()}");
                ITXAsset.FileMediaInfo = null;
                return ITXAsset;
            }
            ITXAsset.FileMediaInfo = processMediaInfoOutput(MIProcess.Output.ToString());
            return ITXAsset;
        }


        private MediaInfo processMediaInfoOutput(string xmlOutput)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlOutput);
            XmlElement root = doc.DocumentElement;
            XmlNamespaceManager namespaceMananger = new XmlNamespaceManager(doc.NameTable);
            namespaceMananger.AddNamespace("myprefix", "https://mediaarea.net/mediainfo");
            MediaInfo details = new MediaInfo();
            details.general_Format = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Format", namespaceMananger)?.InnerXml;
            details.general_AudioTracks = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:AudioCount", namespaceMananger)?.InnerXml;
            details.general_Duration = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Duration", namespaceMananger)?.InnerXml;
            details.general_Format_Profile = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Format_Profile", namespaceMananger)?.InnerXml;
            details.general_Format_Settings = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Format_Settings", namespaceMananger)?.InnerXml;
            details.general_FrameRate = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:FrameRate", namespaceMananger)?.InnerXml;
            details.general_VideoTracks = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:VideoCount", namespaceMananger)?.InnerXml;

            details.video_Format = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format", namespaceMananger)?.InnerXml;
            details.video_BitDepth = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:BitDepth", namespaceMananger)?.InnerXml;
            details.video_BitRate = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:BitRate", namespaceMananger)?.InnerXml;
            details.video_Format_Level = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format_Level", namespaceMananger)?.InnerXml;
            details.video_Format_Profile = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format_Profile", namespaceMananger)?.InnerXml;
            details.video_Format_Settings_Wrapping = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format_Settings_Wrapping", namespaceMananger)?.InnerXml;
            details.video_Height = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Height", namespaceMananger)?.InnerXml;
            details.video_Width = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Width", namespaceMananger)?.InnerXml;
            details.video_ScanType = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:ScanType", namespaceMananger)?.InnerXml;

            details.audio_Format = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:Format", namespaceMananger)?.InnerXml;
            details.audio_BitDepth = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:BitDepth", namespaceMananger)?.InnerXml;
            details.audio_Channels = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:Channels", namespaceMananger)?.InnerXml;
            details.audio_SamplingRate = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:SamplingRate", namespaceMananger)?.InnerXml;

            XmlNodeList others = root.SelectNodes("//myprefix:media/myprefix:track[@type='Other']", namespaceMananger);
            foreach (XmlNode node in others)
            {
                if (node["TimeCode_Settings"]?.InnerXml == "Material Package")
                {
                    details.other_Timecode = node["TimeCode_FirstFrame"].InnerXml;
                }
            }

            return details;
        }


    }
}
