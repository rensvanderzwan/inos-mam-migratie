﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data;
using Newtonsoft.Json;
using SharedLib;

namespace RunMediaInfoFFProbe
{
    static public class DatabaseIO
    {
        static readonly private string _MyConnString = "server=vs-heditmigratie1.edit.nos.nl;uid=itbo;pwd=diwemww11;database=inosmam_migratie;UseAffectedRows=True;SslMode=None";

        static private bool Open(MySqlConnection Connection)
        {
            try
            {
                Connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
              //  Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Open Mysql connection: {ex.Message}");
                return false;
            }
        }

        static public Asset GetAssetToInfo()
        {
            Guid ingest_guid = Guid.NewGuid();

            string UpdateQuery = @$"UPDATE assets 
                                  SET ingest_guid_dhg = @ingest_guid_dhg 
                                  WHERE
                                     ingest_guid_dhg is null
                                     AND file_location is not null
	                              LIMIT 1";

            string SelectQuery = @$"SELECT 
                                        * 
                                     FROM 
                                        assets
                                     WHERE ingest_guid_dhg = @ingest_guid_dhg";


            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return null;

                using (var sc = new MySqlCommand(UpdateQuery, sq))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@ingest_guid_dhg", ingest_guid);
                        int Updated = sc.ExecuteNonQuery();   // 1 = new / 2 = update / 0 = not changed
                        if (Updated == 0) return null;  // noting to do
                    }
                    catch (MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to set guid: {ErrorMessage}");
                        return null;
                    }
                }


                using (var sc = new MySqlCommand(SelectQuery, sq))
                {
                    sc.Parameters.AddWithValue($"@ingest_guid_dhg", ingest_guid);
                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        try
                        {
                            Asset asset = new Asset() { Assetdata = new AssetData() };
                            while (mySQlReader.Read())
                            {
                                asset.AssetID = Convert.ToInt64(mySQlReader["itx_asset_id"]);
                                asset.Assetdata.AssetID = asset.AssetID;
                                asset.Assetdata.AssetName = mySQlReader.GetString("guci");
                                asset.Assetdata.AssetTitle = mySQlReader.GetString("title");
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("file_location")))
                                    asset.Assetdata.MediaLocation = mySQlReader.GetString("file_location");
                            }
                            return asset;
                        }
                        catch (MySqlException ex)
                        {
                            string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                            Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to get asset: {ErrorMessage}");
                            return null;
                        }
                        catch (NullReferenceException ex)
                        {
                            string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                            Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to get asset: {ErrorMessage}");
                            return null;
                        }
                        catch (Exception ex)
                        {
                            string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                            Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to get asset: {ErrorMessage}");
                            return null;
                        }
                    }
                }
            }
        }





        static public Asset GetAssets()
        {
            long RandomNum = new Random().Next(0, 3000000);
            long RandomBool = new Random().Next(1, 3);
            string LargerSmaller;
            if ( RandomBool == 1)
            {
                LargerSmaller = ">";
            }
            else
            {
                LargerSmaller = "<";
            }
            string Query = $"SELECT * FROM assets WHERE file_location is not null and file_media_info is null limit {RandomNum}, 1;";

            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return null;

                using (var sc = new MySqlCommand(Query, sq))
                {
                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        Asset asset = new Asset() { Assetdata = new AssetData() };
                        while (mySQlReader.Read())
                        {
                            asset.Assetdata.AssetID = Convert.ToInt64( mySQlReader["itx_asset_id"]);
                            asset.AssetID = Convert.ToInt64( mySQlReader["itx_asset_id"]);
                            asset.Assetdata.MediaLocation = mySQlReader["file_location"].ToString();
                            asset.Assetdata.AssetName = mySQlReader["guci"].ToString();
                            
                        }
                        return asset;
                    }
                }
            }
        }





        static public int UpdateAsset(Asset ITXAsset)
        {
            // if ingestlocation is not set than its an update from the InsertTo-DB action
            string Query = @$"UPDATE assets SET 
                                file_media_info = COALESCE(@file_media_info, file_media_info),
                                record_updated = @record_updated
                             WHERE itx_asset_id = @itx_asset_id";

            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return 0;

                using (var sc = new MySqlCommand(Query, sq))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@itx_asset_id", ITXAsset.AssetID);
                        sc.Parameters.AddWithValue("@file_media_info", JsonConvert.SerializeObject(ITXAsset.FileMediaInfo, Formatting.Indented));
                        sc.Parameters.AddWithValue("@record_updated", DateTime.UtcNow);

                        int Updated = sc.ExecuteNonQuery();   // number of rows affected by 
                       Logger.WriteConsoleAndLog(MsgType.INFO, "Update", $"AssetId:{ITXAsset.AssetID} Updated:{Updated.ToString()} Name:{ITXAsset.Assetdata.AssetName}");
                        return Updated;
                    }
                    catch (MySqlException ex)
                    {
                        var ErrorMessage = $"Message: {ex.Message} InnerException1: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"UpdateAsset1: {ITXAsset.AssetID} {ErrorMessage}");
                        return 0;
                    }
                    catch (NullReferenceException ex)
                    {
                        var ErrorMessage = $"Message: {ex.Message} InnerException2: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"UpdateAsset2: {ITXAsset.AssetID} {ErrorMessage}");
                        return 0;
                    }
                    catch (AggregateException ex)
                    {
                        var ErrorMessage = $"Message: {ex.Message} InnerException2: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"UpdateAsset3: {ITXAsset.AssetID} {ErrorMessage}");
                        return 0;
                    }
                    catch (Exception ex)
                    {
                        // Added a final exception in case the above does not catch the nullreference exception.
                        var ErrorMessage = $"Message: {ex.Message} InnerException3: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"UpdateJob4: {ITXAsset.AssetID} {ErrorMessage}");
                        return 0;
                    }
                }
            }
        }
    }
}
