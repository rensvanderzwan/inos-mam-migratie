﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using Org.BouncyCastle.Asn1.Cmp;
using SharedLib;
using System.Configuration;

namespace IngestAssetIntoAvid
{
    public class Main
    {
        private static NameValueCollection _appSettings;
        List<MDProcess> _mdProcessList = new List<MDProcess>();
        string _ingestLocation;
        private string _mdHost1;
        private string _mdHost2;
        private MediaDirector _mediaDirector1; 
        private MediaDirector _mediaDirector2;
        private int _minHour;
        private int _maxHour;
        private int _minJobs;
        private int _maxJobs;
        public Main()
        {
         //   _appSettings = ConfigurationManager.AppSettings;
            _ingestLocation = ConfigurationManager.AppSettings["ingest_location"];
            _mdHost1 = ConfigurationManager.AppSettings[$"md_host_{_ingestLocation}1"];
            _mdHost2 = ConfigurationManager.AppSettings[$"md_host_{_ingestLocation}2"];


            Logger._LogPath = ConfigurationManager.AppSettings[$"log_path_{_ingestLocation}"];

            _mediaDirector1 = new MediaDirector(_mdHost1, "Admin", "admin");
            _mediaDirector2 = new MediaDirector(_mdHost2, "Admin", "admin");
        }

        public void ConsoleStart()
        {
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Start App: ###############");
            Forever();
        }

        public void TaskStart()
        {
            Task.Factory.StartNew(() => Forever());
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Start Service: ###############");
        }

        public void TaskStop()
        {
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Stop Service: ###############");
        }


        private void Forever()
        {
            CancellationTokenSource cancelSource = new CancellationTokenSource();
            try
            {
                do
                {
                    Logger.RotateLog();
                    Start();
                } while (cancelSource.Token.WaitHandle.WaitOne(8 * 1000) || true);    // wait before diving into the loop again.
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", ex.ToString());
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", $"{ex.Message}");
                Environment.Exit(1);
            }
        }

        public void Start()
        {
            updateRunningProcesses();
            
            // When enable_ingest is set to false in config the service stops adding new jobs.
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");

            // Throttle the max numer of parallel Jobs down between peak hours
            _minJobs = Convert.ToInt32(ConfigurationManager.AppSettings[$"min_joblimit"]);
            _maxJobs = Convert.ToInt32(ConfigurationManager.AppSettings[$"max_joblimit"]);
            string[] PeakHours = ConfigurationManager.AppSettings[$"peak_time"].Split(',');
            _minHour = DateTime.Parse(PeakHours[0]).Hour;
            _maxHour = DateTime.Parse(PeakHours[1]).Hour;

            int ParallelJobs = 4;
            TimeSpan timeOfDay = DateTime.Now.TimeOfDay;  // Get current hour in 24 hrs format;
            if (timeOfDay.Hours >= _minHour && timeOfDay.Hours < _maxHour)
            {
                ParallelJobs = _minJobs;
            }
            else
            {
                ParallelJobs = _maxJobs;
            }



            bool EnableIngest = Convert.ToBoolean((ConfigurationManager.AppSettings["enable_ingest"]));
            bool MdHost1Enabled = Convert.ToBoolean(ConfigurationManager.AppSettings[$"md_host_{_ingestLocation}1_enabled"]);
            bool MdHost2Enabled = Convert.ToBoolean(ConfigurationManager.AppSettings[$"md_host_{_ingestLocation}2_enabled"]);
            if (EnableIngest == false)
                return;
            if(_mdProcessList.Where(x=>x.HostName == _mediaDirector1._mdUrl).Count() < ParallelJobs && MdHost1Enabled == true)
            {
                Asset asset = DatabaseIO.GetAssetToTransfer(_ingestLocation);
                if (asset == null)               // no new assets to transfer
                {
                    Thread.Sleep(10000);
                    return;
                }
                asset = TransformMetadata(asset);
                string DivisionFolder = GenericFunctions.GetDivision(asset);
                string YearFolder = GenericFunctions.GetYearFolder(asset);
                string YearSubFolder = GenericFunctions.GetYearSubfolder(asset);
                string NexisLocation = GetNexisLocation(asset, _ingestLocation);
                List<CustomInterplayAttribute> CustomAttributes = ComposeCustomAttributes(asset);

                string processUrl = _mediaDirector1.ingestFromOp1a(
                                    asset.Assetdata.MediaLocation,
                                    asset.Assetdata.AssetName,
                                    $"Catalogs/iNOS-MAM Archief/{DivisionFolder}/{YearFolder}/{YearSubFolder}",
                                    $@"\\{NexisLocation}\Avid MediaFiles\MXF\{{System.Hostname}}_{{System.Volume.Date.Extended}}",
                                    CustomAttributes
                                 );
                _mdProcessList.Add(new MDProcess() { Url = processUrl, HostName = _mediaDirector1._mdUrl, ItxAssetID = asset.AssetID, ItxAssetName = asset.Assetdata.AssetName, StartIngest = DateTime.UtcNow });
                Logger.WriteConsoleAndLog(MsgType.INFO, asset.AssetID.ToString(), $"Ingest start md1: {asset.Assetdata.AssetName} processurl: {processUrl}");
            }

            if (_mdProcessList.Where(x => x.HostName == _mediaDirector2._mdUrl).Count() < ParallelJobs && MdHost2Enabled == true)
            {
                Asset asset = DatabaseIO.GetAssetToTransfer(_ingestLocation);
                if (asset == null)               // no new assets to transfer
                {
                    Thread.Sleep(10000);
                    return;
                }
                asset = TransformMetadata(asset);
                string DivisionFolder = GenericFunctions.GetDivision(asset);
                string YearFolder = GenericFunctions.GetYearFolder(asset);
                string YearSubFolder = GenericFunctions.GetYearSubfolder(asset);
                string NexisLocation = GetNexisLocation(asset, _ingestLocation);
                List<CustomInterplayAttribute> CustomAttributes = ComposeCustomAttributes(asset);

                string processUrl = _mediaDirector2.ingestFromOp1a(
                      asset.Assetdata.MediaLocation,
                      asset.Assetdata.AssetName,
                      $"Catalogs/iNOS-MAM Archief/{DivisionFolder}/{YearFolder}/{YearSubFolder}",
                      $@"\\{NexisLocation}\Avid MediaFiles\MXF\{{System.Hostname}}_{{System.Volume.Date.Extended}}",
                      CustomAttributes
                   );
                _mdProcessList.Add(new MDProcess() { Url = processUrl, HostName = _mediaDirector2._mdUrl, ItxAssetID = asset.AssetID, ItxAssetName = asset.Assetdata.AssetName ,StartIngest = DateTime.UtcNow });
                Logger.WriteConsoleAndLog(MsgType.INFO, asset.AssetID.ToString(), $"Ingest start md2: {asset.Assetdata.AssetName} processurl: {processUrl}");
            }
        }

        private Asset TransformMetadata(Asset asset)
        {
            // if any data needs to change do it here
            return asset;
        }

        private List<CustomInterplayAttribute> ComposeCustomAttributes(Asset asset)
        {
            List<CustomInterplayAttribute> CustomAttributes = new List<CustomInterplayAttribute>();
            CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS_Clip_Content_Type", value = asset.Assetdata.ContentType });
            DateTime? vervalDatum = null;
            if(asset.Assetdata.ArchiveMetadataNews != null)
            {
                vervalDatum = asset.Assetdata.ArchiveMetadataNews.VervalDatum;
            }
            else if(asset.Assetdata.ArchiveMetaDataSport != null)
            {
                vervalDatum = asset.Assetdata.ArchiveMetaDataSport.VervalDatum;
            }
            if(vervalDatum != null)
            {
                CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Vervaldatum", value =  ((DateTime) vervalDatum).ToString("yyyy-MM-dd") });
            }

            DateTime? verhaalDatum = null;
            if(asset.Assetdata.InosMetadata != null)
            {
                verhaalDatum = asset.Assetdata.InosMetadata.InosVerhaalDatum;
            }
            if(verhaalDatum != null)
            {
                CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Verhaaldatum", value = ((DateTime)verhaalDatum).ToString("yyy-MM-dd") });
            }

            if(asset.Assetdata.ArchiveMetaDataSport?.RechtenVrij != null)
            {
                string RightsColor = asset.Assetdata.ArchiveMetaDataSport?.RechtenVrij.ToString();
                if (RightsColor == RechtenVrij_EN.Green.ToString())
                {
                    CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechtenvrij", value = RechtenVrij_NL.Groen.ToString() });
                }
                else if (RightsColor == RechtenVrij_EN.Amber.ToString())
                {
                    CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechtenvrij", value = RechtenVrij_NL.Oranje.ToString() });
                }
                else if (RightsColor == RechtenVrij_EN.Red.ToString())
                {
                    CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechtenvrij", value = RechtenVrij_NL.Rood.ToString() });
                }
                else
                {

                }
            }
            else if(asset.Assetdata.ProductionMetadataNews?.RechtenVrij != null)
            {
                string RightsColor = asset.Assetdata.ProductionMetadataNews?.RechtenVrij.ToString();
                if (RightsColor == RechtenVrij_EN.Green.ToString())
                {
                    CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechtenvrij", value = RechtenVrij_NL.Groen.ToString() });
                }
                else if (RightsColor == RechtenVrij_EN.Amber.ToString())
                {
                    CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechtenvrij", value = RechtenVrij_NL.Oranje.ToString() });
                }
                else if (RightsColor == RechtenVrij_EN.Red.ToString())
                {
                    CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechtenvrij", value = RechtenVrij_NL.Rood.ToString() });
                }
                else
                {

                }
            }
            
            CustomAttributes.Add(new CustomInterplayAttribute() { name = "Video ID", value = asset.AssetID.ToString() });

            CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS_Clip_Backup_Status", value = "True" });

            CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Redactie", value = GenericFunctions.GetDivision(asset) });

            if (!string.IsNullOrWhiteSpace(asset.Assetdata.ProductionMetadataNews?.Rechten))
            {
                CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechten", value = asset.Assetdata.ProductionMetadataNews.Rechten });
            }
            else if (!string.IsNullOrWhiteSpace(asset.Assetdata.ArchiveMetaDataSport?.Rechten))
            {
                CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Rechten", value = asset.Assetdata.ArchiveMetaDataSport.Rechten });
            }

            if (asset.Assetdata.AssetTitle != null)
            {
                string Title = asset.Assetdata.AssetTitle;
                CustomAttributes.Add(new CustomInterplayAttribute() { name = "iNOS Titel", value = Title });
            }

            return CustomAttributes;
        }

 

 

        private string GetNexisLocation(Asset asset, string IngestLocation)
        {
            string NexisHost = "s-heditvsd2";
            if(IngestLocation == "hlv")
            {
                if(asset.Assetdata.ContentType.Substring(1, 2) == "SI")
                {
                    NexisHost = @"s-heditvsd\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "ST")
                {
                    NexisHost = @"s-heditvsd\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SK")
                {
                    NexisHost = @"s-heditvsd2\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SD")
                {
                    NexisHost = @"s-heditvsd2\Archief-media";
                }


                else if (asset.Assetdata.ContentType.Substring(1, 2) == "NI")
                {
                    NexisHost = @"s-heditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(2, 1) == "G")
                {
                    NexisHost = @"s-heditvsd2\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(2, 1) == "A")
                {
                    NexisHost = @"s-heditvsd2\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(2, 1) == "P")
                {
                    NexisHost = @"s-heditvsd2\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "NV")
                {
                    NexisHost = @"s-heditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SV")
                {
                    NexisHost = @"s-heditvsd\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "UV")
                {
                    NexisHost = @"s-heditvsd\Nieuwsuur-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "YV")
                {
                    NexisHost = @"s-heditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "CV")
                {
                    NexisHost = @"s-heditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "HV")
                {
                    NexisHost = @"s-heditvsd\Nieuws-media";
                }
            }
            else if(IngestLocation == "dhg")
            {
                if (asset.Assetdata.ContentType.Substring(1, 2) == "SI")
                {
                    NexisHost = @"s-deditvsd\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "ST")
                {
                    NexisHost = @"s-deditvsd\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SK")
                {
                    NexisHost = @"s-deditvsd\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SD")
                {
                    NexisHost = @"s-deditvsd\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "NI")
                {
                    NexisHost = @"s-deditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(2, 1) == "G")
                {
                    NexisHost = @"s-deditvsd\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(2, 1) == "A")
                {
                    NexisHost = @"s-deditvsd\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(2, 1) == "P")
                {
                    NexisHost = @"s-deditvsd\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "NV")
                {
                    NexisHost = @"s-deditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SV")
                {
                    NexisHost = @"s-deditvsd\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "UV")
                {
                    NexisHost = @"s-deditvsd\Nieuwsuur-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "YV")
                {
                    NexisHost = @"s-deditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "CV")
                {
                    NexisHost = @"s-deditvsd\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "HV")
                {
                    NexisHost = @"s-deditvsd\Nieuws-media";
                }
            }
            else if (IngestLocation == "tst")
            {
                if (asset.Assetdata.ContentType.Substring(1, 2) == "SI")
                {
                    NexisHost = @"s-heditvsd-tst\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "ST")
                {
                    NexisHost = @"s-heditvsd-tst\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SK")
                {
                    NexisHost = @"s-heditvsd-tst\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SD")
                {
                    NexisHost = @"s-heditvsd-tst\Archief-media";
                }
                if (asset.Assetdata.ContentType.Substring(2, 1) == "G")
                {
                    NexisHost = @"s-heditvsd-tst\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(2, 1) == "A")
                {
                    NexisHost = @"s-heditvsd-tst\Archief-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "NV")
                {
                    NexisHost = @"s-heditvsd-tst\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SV")
                {
                    NexisHost = @"s-heditvsd-tst\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "UV")
                {
                    NexisHost = @"s-heditvsd-tst\Nieuwsuur-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "YV")
                {
                    NexisHost = @"s-heditvsd-tst\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "CV")
                {
                    NexisHost = @"s-heditvsd-tst\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "HV")
                {
                    NexisHost = @"s-heditvsd-tst\Nieuws-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "SI")
                {
                    NexisHost = @"s-heditvsd-tst\Sport-media";
                }
                else if (asset.Assetdata.ContentType.Substring(1, 2) == "ST")
                {
                    NexisHost = @"s-heditvsd-tst\Sport-media";
                }
                else
                {
                    NexisHost = @"s-deditvsd-tst\Nieuws-media";
                }
            }
                return NexisHost;
        }

        public void updateRunningProcesses()
        {
            foreach (MDProcess process in _mdProcessList)
            {
                bool result = false;
                string response = "";
                if(process.HostName == _mediaDirector1._mdUrl)
                    result = _mediaDirector1.getProcess(process.Url, out response);
                else if(process.HostName == _mediaDirector2._mdUrl)
                    result = _mediaDirector2.getProcess(process.Url, out response);
                if (result == false) 
                    continue;

                MDProcess updatedProcess = ParseProcessUrl(response);
                process.State = updatedProcess.State;
                process.Percent = updatedProcess.Percent;
                process.Message = updatedProcess.Message;
                process.ErrorMessage = updatedProcess.ErrorMessage;
                process.Summary = updatedProcess.Summary;
                process.MobId = updatedProcess.MobId;


                int succes = DatabaseIO.UpdateAsset(new Asset()
                {
                    IngestLocation = _ingestLocation,
                    AssetID = process.ItxAssetID,
                    IngestStatus = process.State,
                    IngestProgress = Convert.ToInt32(process.Percent),
                    IngestMessage = process.Message,
                    IngestErrorMessage = process.ErrorMessage,
                    IngestStarted = process.StartIngest,
                    InterplayMobID = process.MobId,
                    Assetdata = new AssetData() { AssetID = process.ItxAssetID , AssetName = process.ItxAssetName}
                }) ;

                if (process.State == "finished")
                    Logger.WriteConsoleAndLog(MsgType.INFO, process.ItxAssetID.ToString(), $"Ingest Finished succesfull {process.ItxAssetName}");
                else if(process.State == "error")
                    Logger.WriteConsoleAndLog(MsgType.ERROR, process.ItxAssetID.ToString(), $"Ingest Finished with errors {process.ItxAssetName} {process.ErrorMessage}");

            }

            _mdProcessList.RemoveAll(x => ( x.State ==  "finished" || x.State == "error"));
        }

        private MDProcess ParseProcessUrl(string Url)
        {
            MDProcess mdProcess = new MDProcess();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Url);
            XmlElement root = doc.DocumentElement;
            mdProcess.Url = root.SelectSingleNode("/process").Attributes["href"].Value;
            mdProcess.State = root.SelectSingleNode("/process/progress").Attributes["state"].Value;
            mdProcess.Group = root.SelectSingleNode("/process/progress").Attributes["group"].Value;
            mdProcess.Percent = root.SelectSingleNode("/process/progress").Attributes["percent"].Value;
            mdProcess.Summary = root.SelectSingleNode("/process/progress/summary").InnerText;
            mdProcess.Message = root.SelectSingleNode("/process/progress/message").InnerText;
            if(!string.IsNullOrEmpty(root.SelectSingleNode("/process/progress/errorMessage").InnerText))
            {
                mdProcess.ErrorMessage = root.SelectSingleNode("/process/progress/errorMessage").InnerText;
            }
            string InterplayUri = root.SelectSingleNode("/process/progress/progressDetail/clipList/clip/interplayUri")?.InnerText;
            if(!string.IsNullOrEmpty(InterplayUri))
            {
                mdProcess.MobId = Regex.Replace(InterplayUri, @"^.*\?mobid=", "");
            }
            // interplay://AvidWorkgroup?mobid=060a2b340101010101010f0013-000000-d2a409009e8d44e7-9bad66e0c584-6389
            
            return mdProcess;
        }
    }
}
