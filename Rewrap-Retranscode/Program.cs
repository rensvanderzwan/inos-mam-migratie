﻿using System;
using System.Collections.Generic;
using SharedLib;
// retranscode a clip based on guci and UNC Path.

namespace Rewrap_Retranscode
{
    class Arguments
    {
        public string Path { get; set; }
        public string startTimecode { get; set; }
        public string guci { get; set; }
    }

    class Program
    {
        static List<Arguments> _args = new List<Arguments>();
        static void Main(string[] args)
        {
            Logger._LogPath = @"C:\Temp\RewrapTranscode.log";

            _args.Add(new Arguments() { guci = "archief-andere-omroepen-1-CSI200420IY", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\4_20\archief-andere-omroepen-1-CSI200420IY_OMN0420220827.mxf", startTimecode = "00:01:57:01" });
            _args.Add(new Arguments() { guci = "Beelden-zeilers-op-Mallorca-1-CSI200316UC", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\3_18\Beelden-zeilers-op-Mallorca-1-CSI200316UC_OMN0318082151.mxf", startTimecode = "00:00:00:00" });
            _args.Add(new Arguments() { guci = "Groepsfase-eerste-speelronde--Nederland----CSI200406JV", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\4_8\Groepsfase-eerste-speelronde--Nederland----CSI200406JV_OMN0408084358.mxf", startTimecode = "20:36:59:01" });
            _args.Add(new Arguments() { guci = "Groepsfase-eerste-speelronde--Nederland----CSI200406LU", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\4_8\Groepsfase-eerste-speelronde--Nederland----CSI200406LU_OMN0408084436.mxf", startTimecode = "21:45:50:23" });
            _args.Add(new Arguments() { guci = "Kwartfinale--Nederland---Rusland-CSI200406JN", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\4_8\Kwartfinale--Nederland---Rusland-CSI200406JN_OMN0408084412.mxf", startTimecode = "23:23:48:07" });
            _args.Add(new Arguments() { guci = "Promo-beelden-470-1-1-CSI200316CC", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\3_18\Promo-beelden-470-1-1-CSI200316CC_OMN0318082557.mxf", startTimecode = "19:07:34:16" });
            _args.Add(new Arguments() { guci = "Samenvatting-van-de-wedstrijd-Excelsior----CSI200502TR", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\5_4\Samenvatting-van-de-wedstrijd-Excelsior----CSI200502TR_OMN0504061612.mxf", startTimecode = "00:00:00:00" });
            _args.Add(new Arguments() { guci = "Samenvatting-van-de-wedstrijd-PSV---FC-Utr-CSI200501LD", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\5_1\Samenvatting-van-de-wedstrijd-PSV---FC-Utr-CSI200501LD_OMN0501223349.mxf", startTimecode = "00:00:00:00" });
            _args.Add(new Arguments() { guci = "Samenvatting-van-de-wedstrijd-Willem-II----CSI200501BH", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\5_1\Samenvatting-van-de-wedstrijd-Willem-II----CSI200501BH_OMN0501222715.mxf", startTimecode = "00:08:49:00" });
            _args.Add(new Arguments() { guci = "SPORTJOURNAAL-1-CF-CSI200422", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\4_22\SPORTJOURNAAL-1-CF-CSI200422_OMN0422190632.mxf", startTimecode = "18:39:58:16" });
            _args.Add(new Arguments() { guci = "WeTransfer-materiaal-Bouwmeester-1-CSI200324PG", Path = @"\\STOREHLV\CONTENTHLV$\MAM\MAM_ARCHIEF_MEDIA\MEDIA\MXF\2020\3_24\WeTransfer-materiaal-Bouwmeester-1-CSI200324PG_OMN0324161022.mxf", startTimecode = "00:00:00:00" });

            foreach (Arguments Args in _args)
            {
                Logger.WriteConsoleAndLog(MsgType.INFO, null, $"RewrapRetranscode Start {Args.guci}");
                string shellargs = ComposerArgs(Args);

                ShellProcess MIProcess = new ShellProcess("Utilities\\ffmpeg.exe", shellargs, 300, ConsoleCtrlEvent.CTRL_C, true);
                if (MIProcess.Process.ExitCode != 0)
                {
                    Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"RewrapTranscode error: {MIProcess.Process.ExitCode} {MIProcess.Output.ToString()}");
                }
                else
                {
                    Logger.WriteConsoleAndLog(MsgType.INFO, null, $"RewrapRetranscode Finished {Args.guci}");
                }

            }
        }


        static private string ComposerArgs(Arguments Args)
        {
            string OuputParams = "-vcodec mpeg2video -b:v 50M -minrate 50M -maxrate 50M -bufsize 3835k -g 12 -bf 2 -profile:v 0 -level:v 2 -aspect 16:9 -flags ilme -top 1 -pix_fmt yuv422p -field_order tt -map 0:0 -map 0:1 -map 0:2 -map 0:3? -map 0:4? -map 0:5? -map 0:6? -map 0:7? -map 0:8? -acodec pcm_s24le -ac 1 -ar 48000";
            string outPath = @$"\\storehlv\contenthlv$\Medex\MXF_Inbox\Media_to_MAM\{Args.guci}.mxf";

            return $" -i {Args.Path} {OuputParams} -y -timecode {Args.startTimecode} {outPath}";

        }

    }
}
