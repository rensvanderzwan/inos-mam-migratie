﻿using System;
using System.ServiceProcess;

namespace WindowsService_IngestAssetIntoAvid
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var service = new ServiceStarter(""))
            {
                ServiceBase.Run(service);
            }
        }
    }
}
