﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Org.BouncyCastle.Asn1;
using SharedLib;

namespace CSK_CSD_to_CSI_Conversion
{
    class Main
    {

        public Main()
        {
            Logger._LogPath = @"C:\Temp\CSD-CSK-to_CSI-Coversie.log";
        }

        public void ConsoleStart()
        {
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Start App: ###############");
            // Get CSK and CSD's from ITX

            List<Asset> assetList = new List<Asset>(); List<Asset> assetsOnPage;
            int page = 1;
            do
            {
                assetsOnPage = ITX_Rest_Connector.GetMigrationAssetsIDs(page++);
                assetList.AddRange(assetsOnPage);

            } while (assetsOnPage.Count > 0);



            foreach (Asset ITXAsset in assetList)
            {
                ITXAsset.Assetdata = ITX_Rest_Connector.GetMigrationAssetsMetadata(ITXAsset.AssetID);
            }

            // add category
  //          List<Asset> catAssetList = assetList.Where(x => !x.Assetdata.Categories.Contains("CSK-clip")).ToList();
            //foreach (Asset ITXAsset in assetList)
            //{
            //    if (ITXAsset.Assetdata != null)
            //    {
            //        if (ITXAsset.Assetdata.ContentType == "CSD")
            //        {
            //            ITX_Rest_Connector.AddCategory("CSD-clip", ITXAsset);
            //        }
            //        else if (ITXAsset.Assetdata.ContentType == "CSK")
            //        {
            //            ITX_Rest_Connector.AddCategory("CSK-clip", ITXAsset);
            //        }
            //        else
            //        {
            //            Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"ContentType not CSD or CSK {ITXAsset.Assetdata.AssetName}");
            //        }
            //    }
            //}




            // add notitie to the notes in productiemetadata sport
            //foreach (Asset ITXAsset in assetList)
            //{
            //    Console.WriteLine(ITXAsset.Assetdata.AssetName);
            //    if (ITXAsset.Assetdata.ContentType == "CSD")
            //    {
            //        ITX_Rest_Connector.UpdateProductieDataSport("CSD-clip", ITXAsset);
            //    }
            //    else if (ITXAsset.Assetdata.ContentType == "CSK")
            //    {
            //        ITX_Rest_Connector.UpdateProductieDataSport("CSK-clip", ITXAsset);
            //    }
            //    else
            //    {
            //        Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"ContentType not CSD or CSK {ITXAsset.Assetdata.AssetName}");
            //    }
            //}


            // change assetname
            foreach (Asset ITXAsset in assetList)
            {
                if (ITXAsset.Assetdata != null)
                {
                    string newName = Regex.Replace(ITXAsset.Assetdata.AssetName, @"-(CS[DK])(?=\d\d\d\d\d\d{0,2})", "-CSI");
                    if (newName != ITXAsset.Assetdata.AssetName)
                    {
                        ITX_Rest_Connector.ChangeAssetName(newName, ITXAsset);
                    }
                    else
                    {
                        Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"AssetName was not changed {ITXAsset.Assetdata.AssetName}");
                    }
                }
            }


        }

        public void TaskStop()
        {
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Stop Service: ###############");
        }



    }
}
