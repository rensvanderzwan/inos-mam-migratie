﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Correctloggingdata
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Asset> Assets = DatabaseIO.GetAssets();


            List<Asset> LogAssets = Assets.Where(x => x.Assetdata.Logevents.Length > 0).ToList();

            List<Asset> NonPlaceHolders = Assets.Where(x => x.Assetdata.IsPlaceholder == false).ToList();

            foreach(Asset asset in NonPlaceHolders)
            {

                CheckLoggingEvents(asset.Assetdata);
            //    ProcessLoggingdata(asset);

            }

        }


        static public string CheckLoggingEvents(AssetData assetdata)
        {
            // De start van een logevent kan niet kleiner zijn dan de start van een clip.
            // De start+duration van een logevent kan niet groter zijn dan de start+duration van een clip.
            List<string> ErrorDescription = new List<string>();
            double ClipInPoint = TCtoFrames(assetdata.Inpoint);
            double ClipDuration = TCtoFrames(assetdata.Duration);
            bool PastMidnight = ((ClipInPoint + ClipDuration) > 2160000);
            double LoggingInPoint;
            double LoggingDuration;

            foreach (LoggingMetadata loggingdata in assetdata.Logevents)
            {
                LoggingInPoint = TCtoFrames(loggingdata.Inpoint);
                LoggingDuration = TCtoFrames(loggingdata.Duration);

                // Assume if the clips passes midnight and start of logevent <12 hr
                // the logevent is pas midnight too.
                if (PastMidnight && LoggingInPoint < 1080000)
                    LoggingInPoint = TCtoFrames(loggingdata.Inpoint) + 2160000;


                if (LoggingInPoint < ClipInPoint)
                {
                    ErrorDescription.Add($"{assetdata.AssetName} Logging Event {loggingdata.Inpoint} starts before clip inpoint");
                }

                if ((LoggingInPoint + LoggingDuration) > (ClipInPoint + ClipDuration))
                {
                    ErrorDescription.Add($"{assetdata.AssetName} Logging Event {loggingdata.Inpoint} ends past clips endpoint");
                    Console.WriteLine(assetdata.AssetName);
                }
            }
            if (ErrorDescription.Count() > 0)
                return string.Join("\r\n", ErrorDescription);
            else
                return null;
        }




        static private double TCtoFrames(string Timecode, int FrameRate = 25)
        {
            Regex regex = new Regex(@"(\d\d):(\d\d):(\d\d):(\d\d)");
            Match match = regex.Match(Timecode);
            if (match.Success)
            {
                int Hours = Convert.ToInt32(match.Groups[1].Value) * 60 * 60 * FrameRate;
                int Minutes = Convert.ToInt32(match.Groups[2].Value) * 60 * FrameRate;
                int seconds = Convert.ToInt32(match.Groups[3].Value) * FrameRate;
                int Frames = Convert.ToInt32(match.Groups[4].Value);
                return Hours + Minutes + seconds + Frames;
            }
            return 0;
        }

        static private string FramesToTC(double TotalFrames, int Framerate = 25)
        {
            double Hours = Math.Floor(TotalFrames / (60 * 60 * Framerate));
            double Framesleft = TotalFrames - (Hours * Framerate * 60 * 60);
            double Minutes = Math.Floor(Framesleft / (Framerate * 60));
            Framesleft -= Minutes * Framerate * 60;
            double Seconds = Math.Floor(Framesleft / Framerate);
            Framesleft -= Seconds * Framerate;
            int Frames = (int)Math.Floor(Framesleft);
            return $"{Hours.ToString().PadLeft(2, '0')}:{Minutes.ToString().PadLeft(2, '0')}:{Seconds.ToString().PadLeft(2, '0')}:{Frames.ToString().PadLeft(2, '0')}";
        }














        static void ProcessLoggingdata(Asset asset)
        {
            LoggingMetadata[] loggingData = asset.Assetdata.Logevents;

            foreach (LoggingMetadata logitem in loggingData)
            {
                if(logitem.Description == "")
                {
                    Console.WriteLine(asset.Assetdata.AssetName);
                }

                if (loggingData.Where(x => x.Description == logitem.Description).Count() > 5)
                {
                    // we have a duplicate description
                }
                if(loggingData.Where(x => x.Inpoint == logitem.Inpoint).Count() > 1)
                {
                    // we have duplicate inpoint
                }
            }




        }
    }
}
