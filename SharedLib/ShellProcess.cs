﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace SharedLib
{
        public enum ConsoleCtrlEvent
        {
            CTRL_C = 0,
            CTRL_BREAK = 1,
            CTRL_CLOSE = 2,
            CTRL_LOGOFF = 5,
            CTRL_SHUTDOWN = 6
        }
        public class ShellProcess : IShellProcess
        {

            [DllImport("kernel32.dll", SetLastError = true)]
            static extern bool GenerateConsoleCtrlEvent(ConsoleCtrlEvent sigevent, int dwProcessGroupId);

            [DllImport("kernel32.dll")]
            static extern bool SetConsoleCtrlHandler(ConsoleCtrlDelegate handler, bool add);
            delegate Boolean ConsoleCtrlDelegate(ConsoleCtrlEvent type);

            public string Command { get; set; }
            public string Arguments { get; set; }
            public StringBuilder Error { get; set; }
            public StringBuilder Output { get; set; }
            public Exception Exception { get; set; }
            public Process Process { get; set; }
            public bool TimeOut { get; set; }
            Process IShellProcess.Process { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public ShellProcess(string sCommand, string sArguments, int sTimeout, ConsoleCtrlEvent sEvent, bool sDebugMode)
            {
                Command = sCommand;
                Arguments = sArguments;
                Execute(sTimeout, sEvent, sDebugMode);
            }

            public void Execute(int TimeoutMinutes, ConsoleCtrlEvent Event, bool Debug)
            {
                TimeOut = false;
                Process = new Process();
                try
                {
                    Process.StartInfo.FileName = Command;
                    Process.StartInfo.Arguments = Arguments;
                    Process.StartInfo.UseShellExecute = false;
                    Process.StartInfo.RedirectStandardOutput = true;
                    Process.StartInfo.RedirectStandardError = true;

                    Process.EnableRaisingEvents = false;

                    Output = new StringBuilder();
                    Error = new StringBuilder();

                    using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
                    using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
                    {
                        Process.OutputDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                outputWaitHandle.Set();
                            }
                            else
                            {
                                Output.AppendLine(e.Data);
                                if (Debug) Console.WriteLine(e.Data);
                            }
                        };
                        Process.ErrorDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                errorWaitHandle.Set();
                            }
                            else
                            {
                                Error.AppendLine(e.Data);
                            }
                        };

                        Process.Start();

                        Process.BeginOutputReadLine();
                        Process.BeginErrorReadLine();
                        Thread.Sleep(1000); // wait before firing any status, the listener needs a sec to write last progress.

                        if (Process.WaitForExit(TimeoutMinutes * 60 * 1000) &&  // 7 hr
                             outputWaitHandle.WaitOne(TimeoutMinutes * 60 * 1000) &&
                             errorWaitHandle.WaitOne(TimeoutMinutes * 60 * 1000))
                        {
                            if (Process.ExitCode == 0)
                            {

                            }
                            else
                            {

                            }

                            if (!string.IsNullOrEmpty(Error.ToString()))
                            {
                            }
                            if (!string.IsNullOrEmpty(Output.ToString()))
                            {
                            }
                        }
                        else
                        {
                            // Timed out.
                            TimeOut = true;
                            Process.CancelOutputRead();
                            Process.CancelErrorRead();
                            // https://codetitans.pl/blog/post/sending-ctrl-c-signal-to-another-application-on-windows
                            SetConsoleCtrlHandler(null, true);
                            GenerateConsoleCtrlEvent(Event, Process.Id);
                            Process.WaitForExit(20000);
                            Process.Close();
                            Process.Dispose();
                            SetConsoleCtrlHandler(null, false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exception = ex;
                }
            }
        }
}
