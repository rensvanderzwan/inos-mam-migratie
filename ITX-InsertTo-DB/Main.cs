﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using SharedLib;
using System.IO;
using System.Text.RegularExpressions;

namespace iNOS_MAM_Migratie
{
    public class Main
    {
        private NameValueCollection _appSettings;

        public Main()
        {
            _appSettings = System.Configuration.ConfigurationManager.AppSettings;
            Logger._LogPath = _appSettings["log_path"];
        }

        public void ConsoleStart()
        {
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Start App: ###############");
            Forever();
        }

        public void TaskStart()
        {
            Task.Factory.StartNew(() => Forever());
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Start Service: ###############");
        }

        public void TaskStop()
        {
            Logger.WriteConsoleAndLog(MsgType.INFO, null, "############### Stop Service: ###############");
        }


        private void Forever()
        {
            CancellationTokenSource cancelSource = new CancellationTokenSource();
            try
            {
                do
                {
                    Logger.RotateLog();
                    Start(ITXLocation.hlv);
                } while (cancelSource.Token.WaitHandle.WaitOne(1 * 30 * 60 * 1000) || true);    // wait before diving into the loop again.
            }
            catch (Exception ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", ex.ToString());
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Final Exeption: ", $"{ex.Message}");
                Environment.Exit(1);
            }
        }

        public void Start(ITXLocation itxLocation)
        {

            // Get all assets by setting date way into the past.
            List<Asset> itxAssetsFulllist = ITXExportReader.ReadFile(DateTime.Now.AddDays(-100 * 365), itxLocation)?.Take(200000)?.ToList();
            if (itxAssetsFulllist == null)
            {
                return;
            }

            // Get asset ID's from DB
            List<Asset> dbAssetsList = DatabaseIO.GetAssets(false,"hlv", itxLocation);
            if(dbAssetsList == null)
            {
                return;
            }
            Logger.WriteConsoleAndLog(MsgType.INFO, "GetAssets", @$"Found {dbAssetsList.Count()} in database");

            // Get Assets no longer in itxAssetsFulllist since the last run
            List<Asset> NewDeletedAssets = dbAssetsList.Except(itxAssetsFulllist, new IdComparer()).ToList();
            Logger.WriteConsoleAndLog(MsgType.INFO, "DeleteAssets", @$"Found {NewDeletedAssets.Count()} new deleted assets since last run");

            // So flag them in Db as deleted.
            foreach (Asset DeletedAsset in NewDeletedAssets)
            {
                DatabaseIO.UpdateAsset(new Asset() { Assetdata = new AssetData() {AssetName = DeletedAsset.Assetdata.AssetName } , AssetID = DeletedAsset.AssetID, AssetDeleted = true});
            }

            InterplayService.Init(itxLocation.ToString());

            // Insert new assets
            List<Asset> NewAssets = itxAssetsFulllist.Except(dbAssetsList, new IdComparer()).ToList();
            Logger.WriteConsoleAndLog(MsgType.INFO, "NewAssets", @$"Found {NewAssets.Count()} new assets since last run");
            foreach (Asset ITXAsset in NewAssets)
            {
                ITXAsset.Assetdata = ITX_Rest.GetMigrationAssetsMetadata(itxLocation, ITXAsset.AssetID);
                if (ITXAsset.Assetdata != null)
                {
                    ITXAsset.FileMediaInfo = DoMediaInfo(ITXAsset);
                    ITXAsset.InterplayMobID = DoInterplayLookup(ITXAsset);
                    ITXAsset.ITXLocation = itxLocation;
                    DatabaseIO.InsertAsset(ITXAsset);
                }
            }

            // get changed database items
            List<Asset> itxAssetsChangedList = dbAssetsList.Except(itxAssetsFulllist, new LastChangedComparer()).ToList();
            Logger.WriteConsoleAndLog(MsgType.INFO, "ChangedAssets", @$"Found {itxAssetsChangedList.Count()} changed assets since last run");


            // Update existing but changed assets
            foreach (Asset ITXAsset in itxAssetsChangedList)
            {
                ITXAsset.Assetdata = ITX_Rest.GetMigrationAssetsMetadata(itxLocation, ITXAsset.AssetID);
                if (ITXAsset.Assetdata != null)
                {
                    ITXAsset.FileMediaInfo = DoMediaInfo(ITXAsset);
                    ITXAsset.InterplayMobID = DoInterplayLookup(ITXAsset);
                    // record was changed if Updates != 0
                    int Updated = DatabaseIO.UpdateAsset(ITXAsset);
                }
            }
        }

        private MediaInfo DoMediaInfo(Asset ITXAsset)
        {
            if(File.Exists(ITXAsset.Assetdata.MediaLocation))
            {
                ShellProcess MIProcess = new ShellProcess("Utilities\\MediaInfo.exe", "--Output=XML " + $"\"{ITXAsset.Assetdata.MediaLocation}\"", 30, ConsoleCtrlEvent.CTRL_C, true);
                if (MIProcess.Process.ExitCode != 0)
                {
                    Logger.WriteConsoleAndLog(MsgType.ERROR, ITXAsset.AssetID.ToString(), $"MediaInfo error: {MIProcess.Process.ExitCode} {MIProcess.Output.ToString()}");
                    ITXAsset.ErrorDescription = $"MediaInfo error: {MIProcess.Process.ExitCode} {MIProcess.Output.ToString()}";
                    return null;
                }
                return processMediaInfoOutput(MIProcess.Output.ToString());
            }
            else
            {
                return null;
            }
        }

        private string DoInterplayLookup(Asset asset)
        {
            string MobID = InterplayService.NonMigratieMasterClip(InterplayService._interplayUri, "Projects", asset.Assetdata.AssetName);

            Match m = Regex.Match(asset.Assetdata.ContentType, @"C.A|C.G|CSI|CST|CSK|CSD", RegexOptions.IgnoreCase);
            if(m.Success == true)
            {
                return null;
            }
            else
            {
                return MobID;
            }
        }



        private MediaInfo processMediaInfoOutput(string xmlOutput)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlOutput);
            XmlElement root = doc.DocumentElement;
            XmlNamespaceManager namespaceMananger = new XmlNamespaceManager(doc.NameTable);
            namespaceMananger.AddNamespace("myprefix", "https://mediaarea.net/mediainfo");
            MediaInfo details = new MediaInfo();
            details.general_Format = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Format", namespaceMananger)?.InnerXml;
            details.general_AudioTracks = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:AudioCount", namespaceMananger)?.InnerXml;
            details.general_Duration = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Duration", namespaceMananger)?.InnerXml;
            details.general_Format_Profile = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Format_Profile", namespaceMananger)?.InnerXml;
            details.general_Format_Settings = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:Format_Settings", namespaceMananger)?.InnerXml;
            details.general_FrameRate = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:FrameRate", namespaceMananger)?.InnerXml;
            details.general_VideoTracks = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:VideoCount", namespaceMananger)?.InnerXml;
            details.general_FileSize = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='General']/myprefix:FileSize", namespaceMananger)?.InnerXml;

            details.video_Format = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format", namespaceMananger)?.InnerXml;
            details.video_BitDepth = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:BitDepth", namespaceMananger)?.InnerXml;
            details.video_BitRate = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:BitRate", namespaceMananger)?.InnerXml;
            details.video_Format_Level = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format_Level", namespaceMananger)?.InnerXml;
            details.video_Format_Profile = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format_Profile", namespaceMananger)?.InnerXml;
            details.video_Format_Settings_Wrapping = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Format_Settings_Wrapping", namespaceMananger)?.InnerXml;
            details.video_Height = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Height", namespaceMananger)?.InnerXml;
            details.video_Width = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:Width", namespaceMananger)?.InnerXml;
            details.video_ScanType = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Video']/myprefix:ScanType", namespaceMananger)?.InnerXml;

            details.audio_Format = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:Format", namespaceMananger)?.InnerXml;
            details.audio_BitDepth = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:BitDepth", namespaceMananger)?.InnerXml;
            details.audio_Channels = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:Channels", namespaceMananger)?.InnerXml;
            details.audio_SamplingRate = root.SelectSingleNode("//myprefix:media/myprefix:track[@type='Audio']/myprefix:SamplingRate", namespaceMananger)?.InnerXml;

            XmlNodeList others = root.SelectNodes("//myprefix:media/myprefix:track[@type='Other']", namespaceMananger);
            foreach (XmlNode node in others)
            {
                if (node["TimeCode_Settings"]?.InnerXml == "Material Package")
                {
                    details.other_Timecode = node["TimeCode_FirstFrame"].InnerXml;
                }
            }
            return details;
        }
    }

    public class IdComparer : IEqualityComparer<Asset>
    {
        public int GetHashCode(Asset co)
        {
            if (co == null)
            {
                return 0;
            }
            return co.AssetID.GetHashCode();
        }

        public bool Equals(Asset x1, Asset x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) ||
                object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return x1.AssetID == x2.AssetID;
        }
    }




    public class LastChangedComparer : IEqualityComparer<Asset>
    {
        public int GetHashCode(Asset co)
        {
            if (co == null)
            {
                return 0;
            }
            int Hash1 = co.Assetdata.LastModified.GetHashCode();
            int Hash2 = co.AssetID.GetHashCode();
            return Hash1 ^ Hash2;
        }

        public bool Equals(Asset x1, Asset x2)
        {
            if (object.ReferenceEquals(x1, x2))
            {
                return true;
            }
            if (object.ReferenceEquals(x1, null) ||
                object.ReferenceEquals(x2, null))
            {
                return false;
            }
            return x1.Assetdata.LastModified == x2.Assetdata.LastModified && x1.AssetID == x2.AssetID;
        }
    }
}
