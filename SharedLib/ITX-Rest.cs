﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;

namespace SharedLib
{
    public static class ITX_Rest
    {
        static string _locationHLVPrefix = "ITX_HILVERSUM_1_";
        static string _locationDHGPrefix = "ITX_DENHAAG_1_";
        static private string _searchHlvUrl = "http://s-itx-hauto1.itx.nos.nl:8080/OPUS.Interchange/REST/Search";   //   page/#
        static private string _searchDhgUrl = "http://s-itx-dauto1.itx.nos.nl:8080/OPUS.Interchange/REST/Search";   //   page/#
        static private string _assetHlvUrl = "http://s-itx-hauto1.itx.nos.nl:8080/OPUS.Interchange/REST/AssetXml";  //   assetid
        static private string _assetDhgUrl = "http://s-itx-dauto1.itx.nos.nl:8080/OPUS.Interchange/REST/AssetXml";  //   assetid

        static public List<Asset> GetMigrationAssetsIDs(ITXLocation itxLocation, DateTime Startdate, int Page)
        {
            List<Asset> AssetList = new List<Asset>();
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"ITX-Search-Queries\", "SearchForMigrationAssets.xml");
            string SearchBody = File.ReadAllText(path);
            string ModifiedSearchXML = ModifySearchXML(SearchBody, Startdate);

            RestClient client;
            if (itxLocation == ITXLocation.hlv)
                client = new RestClient(_assetHlvUrl);
            else
                client = new RestClient(_assetDhgUrl);

            RestRequest Request = new RestRequest($"Page/{Page}", Method.POST);
            Request.AddParameter("Content-Type", "text/plain", ParameterType.HttpHeader);
            Request.AddParameter("application/xml", ModifiedSearchXML, ParameterType.RequestBody);
            IRestResponse Response = client.Execute(Request);
            if (Response.IsSuccessful)
            {
                AssetList = ParseIds(Response.Content);
            }
            else
            {
                // errors
            }
            return AssetList;
        }

        static public AssetData GetMigrationAssetsMetadata(ITXLocation itxLocation, long AssetID)
        {
            string RequestAssetID = _locationHLVPrefix + AssetID.ToString();

            RestClient client;
            if (itxLocation == ITXLocation.hlv)
                client = new RestClient(_assetHlvUrl);
            else
                client = new RestClient(_assetDhgUrl);

            RestRequest Request = new RestRequest($"/{RequestAssetID}", Method.GET);
            Request.AddParameter("Content-Type", "text/plain", ParameterType.HttpHeader);
            IRestResponse Response = client.Execute(Request);
            if (Response.IsSuccessful)
            {
                return ParseMetadata(Response.Content);
            }
            else
            {
                return null;
            }
        }

        static private AssetData ParseMetadata(string XML)
        {

            var element = XElement.Parse(XML);
            string NoNamespaceXML = RemoveAllNamespaces(element).ToString();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(NoNamespaceXML);
            XmlElement Root = doc.DocumentElement;

            //MediaId

            AssetData assetData = new AssetData();
            assetData.AssetID = Convert.ToInt64(Root.SelectSingleNode("/Asset/MediaId").InnerText);
            assetData.AssetName = Root.SelectSingleNode("/Asset/Name").InnerText.Trim();
            assetData.Guid = Root.SelectSingleNode("/Asset/Guid").InnerText.Trim();
            assetData.AssetTitle = Root.SelectSingleNode("/Asset/Title").InnerText.Trim();
            string Contentype = Root.SelectSingleNode("/Asset/SubType").InnerText.ToUpper().Trim();
            if (Contentype.Length == 3)
                assetData.ContentType = Contentype;
            else
                assetData.ContentType = "???";

            string Duration = Root.SelectSingleNode("/Asset/Duration").InnerText;
            Match durMatch = new Regex(@"^(\d\d:\d\d:\d\d)\.(\d+)$").Match(Duration);
            if (durMatch.Success)
            {
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                nfi.NumberDecimalSeparator = ".";
                string hhmmss = durMatch.Groups[1].Value;
                string fff = "0." + durMatch.Groups[2].Value;
                string frames = ((int)(Convert.ToDecimal(fff, nfi) * 25)).ToString("D2");
                assetData.Duration = hhmmss + ":" + frames;
            }
            else
                assetData.Duration = Duration;

            string Inpoint = Root.SelectSingleNode("/Asset/InPoint").InnerText;
            Match inMatch = new Regex(@"^(\d\d:\d\d:\d\d)\.(\d+)$").Match(Inpoint);
            if(inMatch.Success)
            {
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                nfi.NumberDecimalSeparator = ".";
                string hhmmss = inMatch.Groups[1].Value;
                string fff = "0." + inMatch.Groups[2].Value;
                string frames = ((int) (Convert.ToDecimal(fff, nfi) * 25)).ToString("D2");
                assetData.Inpoint = hhmmss + ":" + frames;
            }
            else
                assetData.Inpoint = Inpoint;

            if( DateTime.TryParse(Root.SelectSingleNode("/Asset/ProductionDate").InnerText,out DateTime ProductionDate) == true )
            {
                assetData.ProductionDate = ProductionDate;
            }
            else
            {
                assetData.ProductionDate = Convert.ToDateTime(Root.SelectSingleNode("/Asset/ProductionDate").InnerText);
            }

            
            assetData.LastModified = Convert.ToDateTime(Root.SelectSingleNode("/Asset/DateLastModified").InnerText);
            assetData.Created = Convert.ToDateTime(Root.SelectSingleNode("/Asset/DateCreated").InnerText);
            assetData.Notes = Root.SelectSingleNode("/Asset/Notes")?.InnerText.Trim();


            List<string> Categories = new List<string>();
            XmlNodeList CategoryNodes = Root.SelectNodes("/Asset/Categories/CategoryName");
            foreach(XmlNode CategoryNode in CategoryNodes)
            {
                Categories.Add(CategoryNode.InnerText);
            }
            assetData.Categories = Categories.ToArray();

            assetData.MediaLocation = Root.SelectSingleNode("/Asset/MediaLocations/Location[LocationType = 'ITX']/MediaPaths/Path[@Protocol = 'SMB']")?.InnerText;
            if (assetData.MediaLocation != null && assetData.MediaLocation.StartsWith(@"\\STOREHLV\CONTENTHLV$\MAM"))
            {
                assetData.IsPlaceholder = false;
            }
            else
            {
                assetData.IsPlaceholder = true;
            }

            assetData.InosMetadata = ParseInosMetadata(Root);
            assetData.ArchiveMetaDataSport = ParseArchiveMetadataSport(Root);
            assetData.ArchiveMetadataNews = ParseArchiveMetadataNews(Root);
            assetData.ProductionMetadataSport = ParseProductionMetadataSport(Root);
            assetData.ProductionMetadataNews = ParseProductionMetadataNews(Root);
            assetData.Logevents = ParseLogevents(Root,assetData.Inpoint,assetData.Duration);

            List<LoggingMetadata> duplicates = assetData.Logevents.GroupBy(s => s).SelectMany(grp => grp.Skip(1)).ToList();
            if (duplicates != null && duplicates.Count > 1)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"duplicate logging events found for {assetData.AssetName}: " + string.Join(",", duplicates.Select(x => x.Inpoint)));
            }

            return assetData;
        }


        static public string CheckLoggingEvents(AssetData assetdata)
        {
            // De start van een logevent kan niet kleiner zijn dan de start van een clip.
            // De start+duration van een logevent kan niet groter zijn dan de start+duration van een clip.
            List<string> ErrorDescription = new List<string>();
            double ClipInPoint = TCtoFrames(assetdata.Inpoint);
            double ClipDuration = TCtoFrames(assetdata.Duration);
            bool PastMidnight = ((ClipInPoint + ClipDuration) > 2160000);
            double LoggingInPoint;
            double LoggingDuration;

            foreach (LoggingMetadata loggingdata in assetdata.Logevents)
            {
                LoggingInPoint = TCtoFrames(loggingdata.Inpoint);
                LoggingDuration = TCtoFrames(loggingdata.Duration);

                // Assume if the clips passes midnight and start of logevent <12 hr
                // the logevent is pas midnight too.
                if (PastMidnight && LoggingInPoint < 1080000)
                    LoggingInPoint = TCtoFrames(loggingdata.Inpoint) + 2160000;


                if(LoggingInPoint < ClipInPoint)
                {
                    ErrorDescription.Add($"{assetdata.AssetName} Logging Event {loggingdata.Inpoint} starts before clip inpoint");
                }

                if((LoggingInPoint + LoggingDuration) > (ClipInPoint + ClipDuration))
                {
                    ErrorDescription.Add($"{assetdata.AssetName} Logging Event {loggingdata.Inpoint} ends past clips endpoint");
                }
            }
            if (ErrorDescription.Count() > 0)
                return string.Join("\r\n", ErrorDescription);
            else
                return null;
        }

        static private long TCtoFrames(string Timecode, int FrameRate = 25)
        {
            Regex regex = new Regex(@"(\d\d):(\d\d):(\d\d):(\d\d)");
            Match match = regex.Match(Timecode);
            if (match.Success)
            {
                int Hours = Convert.ToInt32(match.Groups[1].Value) * 60 * 60 * FrameRate;
                int Minutes = Convert.ToInt32(match.Groups[2].Value) * 60 * FrameRate;
                int seconds = Convert.ToInt32(match.Groups[3].Value) * FrameRate;
                int Frames = Convert.ToInt32(match.Groups[4].Value);
                return Hours + Minutes + seconds + Frames;
            }
            return 0;
        }

        static private string FramesToTC(double TotalFrames, int Framerate = 25)
        {
            double Hours = Math.Floor(TotalFrames / (60 * 60 * Framerate));
            double Framesleft = TotalFrames - (Hours * Framerate * 60 * 60);
            double Minutes = Math.Floor(Framesleft / (Framerate * 60));
            Framesleft -= Minutes * Framerate * 60;
            double Seconds = Math.Floor(Framesleft / Framerate);
            Framesleft -= Seconds * Framerate;
            int Frames = (int)Math.Floor(Framesleft);
            return $"{Hours.ToString().PadLeft(2, '0')}:{Minutes.ToString().PadLeft(2, '0')}:{Seconds.ToString().PadLeft(2, '0')}:{Frames.ToString().PadLeft(2, '0')}";
        }


        static private LoggingMetadata[] ParseLogevents(XmlElement RootElement,string ClipIn, string ClipDur)
        {
            long ClipInPoint = TCtoFrames(ClipIn);
            long ClipDuration = TCtoFrames(ClipDur);
            bool PastMidnight = ((ClipInPoint + ClipDuration) > 2160000);
            long LoggingInPoint;
            long LoggingDuration;

            List<LoggingMetadata> LogMetadata = new List<LoggingMetadata>();
            XmlNodeList Data = RootElement.SelectNodes("/Asset/BusinessMetadata/Metadata/Type[@name = 'Log']/..");
            if (Data != null)
            {
                foreach(XmlNode LogNode in Data)
                {
                    LoggingMetadata logEvent = new LoggingMetadata();
                    logEvent.ITXMetadataID = Convert.ToInt64(LogNode.Attributes["id"].Value);
                    logEvent.Inpoint = LogNode.SelectSingleNode("./InPoint").InnerText;
                    logEvent.Duration = LogNode.SelectSingleNode("./Duration").InnerText;
                    logEvent.Description = LogNode.SelectSingleNode("./Data/Log/Log").InnerText.Trim();


                    LoggingInPoint = TCtoFrames(LogNode.SelectSingleNode("./InPoint").InnerText);
                    LoggingDuration = TCtoFrames(LogNode.SelectSingleNode("./Duration").InnerText);
                    // Assume if the clips passes midnight and start of logevent <12 hr
                    // the logevent is pas midnight too.
                    if (PastMidnight && LoggingInPoint < 1080000)
                    {
                        LoggingInPoint = TCtoFrames(logEvent.Inpoint) + 2160000;
                        logEvent.IsPast24hr = true;
                    }
                    logEvent.FramesFromStart = LoggingInPoint - ClipInPoint;
                    logEvent.Duration2 = LoggingDuration;

                    List<string> RangeWarning = new List<string>();
                    if (LoggingInPoint < ClipInPoint)
                    {
                        RangeWarning.Add( $"Logging Event starts before clip inpoint by {ClipInPoint - LoggingInPoint} frames");
                    }

                    if ((LoggingInPoint + LoggingDuration) > (ClipInPoint + ClipDuration))
                    {
                        RangeWarning.Add( $"Logging Event ends after clip outpoint by {(LoggingInPoint + LoggingDuration) - (ClipInPoint + ClipDuration)} frames");
                    }
                    logEvent.RangeWarning = string.Join("\r\n", RangeWarning);

                    // don't add empty events
                    if ( !string.IsNullOrWhiteSpace(logEvent.Description))
                        LogMetadata.Add(logEvent);
                }
                return LogMetadata.ToArray();
            }
            return null;
        }


        static private ArchiveMetaDataSport ParseArchiveMetadataSport(XmlElement RootElement)
        {
            ArchiveMetaDataSport ArchiveMetadata = new ArchiveMetaDataSport();
            XmlNode Data = RootElement.SelectSingleNode("/Asset/BusinessMetadata/Metadata/Type[@name = 'Archiefdata Sport']/../Data/ArchiefdataSport");
            if (Data != null)
            {
                XmlNode RechtenvrijNode = Data.SelectSingleNode("./Rechtenvrij");
                if (RechtenvrijNode != null && RechtenvrijNode.InnerText != "")
                {
                    Enum.TryParse(RechtenvrijNode.InnerText, true, out RechtenVrij_EN RechtenVrij);
                    ArchiveMetadata.RechtenVrij = RechtenVrij;
                }

                ArchiveMetadata.Onderwerp = Data.SelectSingleNode("./Onderwerp")?.InnerText.Trim();
                ArchiveMetadata.Locatie = Data.SelectSingleNode("./Locatie")?.InnerText.Trim();
                ArchiveMetadata.OpnameDatum = Convert.ToDateTime(Data.SelectSingleNode("./Opnamedatum").InnerText);
                ArchiveMetadata.Rechten = Data.SelectSingleNode("./Rechten")?.InnerText.Trim();
                ArchiveMetadata.Verslaggever = Data.SelectSingleNode("./Verslaggever")?.InnerText.Trim();

                XmlNode GeluidNode = Data.SelectSingleNode(".//Geluid");
                if (GeluidNode != null && GeluidNode.InnerText != "")
                {
                    Enum.TryParse(GeluidNode.InnerText, true , out Geluid geluid);
                    ArchiveMetadata.Geluid = geluid;
                }

                ArchiveMetadata.Tape = Data.SelectSingleNode(".//Tape")?.InnerText.Trim();

                string TrefwoordenList = Data.SelectSingleNode(".//Trefwoorden")?.InnerText;
                if(!string.IsNullOrWhiteSpace(TrefwoordenList))
                {
                    ArchiveMetadata.Trefwoorden = TrefwoordenList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                XmlNode vervalDatumNode = Data.SelectSingleNode(".//Vervaldatum");
                if (vervalDatumNode != null && vervalDatumNode.InnerText != "")
                    ArchiveMetadata.VervalDatum = Convert.ToDateTime(Data.SelectSingleNode(".//Vervaldatum").InnerText);

                return ArchiveMetadata;
            }
            return null;
        }

        static private ArchiveMetaDataNews ParseArchiveMetadataNews(XmlElement RootElement)
        {
            ArchiveMetaDataNews ArchiveMetadata = new ArchiveMetaDataNews();
            XmlNode Data = RootElement.SelectSingleNode("/Asset/BusinessMetadata/Metadata/Type[@name = 'Archiefdata Nieuws']/../Data/ArchiefdataNieuws");
            if (Data != null)
            {
                ArchiveMetadata.Archivaris = Data.SelectSingleNode("./Archivaris")?.InnerText.Trim();

                XmlNode GArchiefStatusNode = Data.SelectSingleNode(".//Archiefstatus");
                if (GArchiefStatusNode != null && GArchiefStatusNode.InnerText != "")
                {
                    Enum.TryParse(GArchiefStatusNode.InnerText, true, out ArchiefStatus archiefStatus);
                    ArchiveMetadata.Status = archiefStatus;
                }

                string TrefwoordenList = Data.SelectSingleNode(".//TrefwoordNieuws")?.InnerText;
                if (!string.IsNullOrWhiteSpace(TrefwoordenList))
                {
                    ArchiveMetadata.Trefwoorden = TrefwoordenList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                string PersonenList = Data.SelectSingleNode(".//Personen")?.InnerText;
                if (!string.IsNullOrWhiteSpace(PersonenList))
                {
                    ArchiveMetadata.Personen = PersonenList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                string SprekerList = Data.SelectSingleNode(".//Spreker")?.InnerText;
                if (!string.IsNullOrWhiteSpace(SprekerList))
                {
                    ArchiveMetadata.Spreker = SprekerList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                ArchiveMetadata.Vegetatie = Data.SelectSingleNode("./Vegetatie")?.InnerText.Trim();
                ArchiveMetadata.Weer = Data.SelectSingleNode("./Weer")?.InnerText.Trim();
                ArchiveMetadata.Beschrijving = Data.SelectSingleNode("./Beschrijving")?.InnerText.Trim();

                XmlNode vervalDatumNode = Data.SelectSingleNode(".//Vervaldatum");
                if (vervalDatumNode != null && vervalDatumNode.InnerText != "")
                    ArchiveMetadata.VervalDatum = Convert.ToDateTime(Data.SelectSingleNode(".//Vervaldatum").InnerText);

                return ArchiveMetadata;
            }
            return null;
        }

        static private ProductionMetadataSport ParseProductionMetadataSport(XmlElement RootElement)
        {
            ProductionMetadataSport ProductionMetadata = new ProductionMetadataSport();
            XmlNode Data = RootElement.SelectSingleNode("/Asset/BusinessMetadata/Metadata/Type[@name = 'Productiedata Sport']/../Data/ProductiedataSport");
            if (Data != null)
            {
                XmlNode RechtenvrijNode = Data.SelectSingleNode("./Rechtenvrij");
                if (RechtenvrijNode != null && RechtenvrijNode.InnerText != "")
                {
                    Enum.TryParse(RechtenvrijNode.InnerText, true, out RechtenVrij_EN RechtenVrij);
                    ProductionMetadata.RechtenVrij = RechtenVrij;
                }

                ProductionMetadata.Rechten = Data.SelectSingleNode("./Rechten")?.InnerText.Trim();
                ProductionMetadata.Sport = Data.SelectSingleNode("./Sport")?.InnerText.Trim();
                ProductionMetadata.Evenement = Data.SelectSingleNode("./Evenement")?.InnerText.Trim();
                ProductionMetadata.Locatie = Data.SelectSingleNode("./Locatie")?.InnerText.Trim();

                XmlNode opnameDatumNode = Data.SelectSingleNode(".//Opnamedatum");
                if (opnameDatumNode != null && opnameDatumNode.InnerText != "")
                    ProductionMetadata.OpnameDatum = Convert.ToDateTime(Data.SelectSingleNode(".//Opnamedatum").InnerText);

                ProductionMetadata.Verslaggever = Data.SelectSingleNode("./Verslaggever")?.InnerText.Trim();

                string RedacteurenList = Data.SelectSingleNode(".//Redacteuren")?.InnerText;
                if (!string.IsNullOrWhiteSpace(RedacteurenList))
                {
                    ProductionMetadata.Redacteuren = RedacteurenList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                ProductionMetadata.Wisbeleid = Data.SelectSingleNode("./Wisbeleid")?.InnerText.Trim();
                ProductionMetadata.Notities = Data.SelectSingleNode("./Notities")?.InnerText.Trim();

                return ProductionMetadata;
            }
            return null;
        }

        static private ProductionMetataNews ParseProductionMetadataNews(XmlElement RootElement)
        {
            ProductionMetataNews ProductionMetadata = new ProductionMetataNews();
            XmlNode Data = RootElement.SelectSingleNode("/Asset/BusinessMetadata/Metadata/Type[@name = 'Productiedata Nieuws']/../Data/ProductiedataNieuws");
            if (Data != null)
            {
                ProductionMetadata.Redactie = Data.SelectSingleNode("./Redactie")?.InnerText.Trim();

                string LocatieList = Data.SelectSingleNode(".//Locatie")?.InnerText;
                if (!string.IsNullOrWhiteSpace(LocatieList))
                {
                    ProductionMetadata.Locatie = LocatieList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                string VerslaggeverList = Data.SelectSingleNode(".//Verslaggever")?.InnerText;
                if (!string.IsNullOrWhiteSpace(VerslaggeverList))
                {
                    ProductionMetadata.Verslaggever = VerslaggeverList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                ProductionMetadata.CameraPloeg = Data.SelectSingleNode("./Cameraploeg")?.InnerText;

                XmlNode opnameDatumNode = Data.SelectSingleNode(".//Opnamedatum");
                if (opnameDatumNode != null && opnameDatumNode.InnerText != "")
                    ProductionMetadata.OpnameDatum = Convert.ToDateTime(Data.SelectSingleNode(".//Opnamedatum").InnerText);

                XmlNode RechtenvrijNode = Data.SelectSingleNode("./Rechten_nos");
                if (RechtenvrijNode != null && RechtenvrijNode.InnerText != "")
                {
                    
                    Enum.TryParse(RechtenvrijNode.InnerText, true, out RechtenVrij_EN RechtenVrij);
                    ProductionMetadata.RechtenVrij = RechtenVrij;
                }

                ProductionMetadata.Rechten = Data.SelectSingleNode("./Rechten")?.InnerText;
                ProductionMetadata.VerhaalNaam = Data.SelectSingleNode("./Verhaalnaam")?.InnerText;

                XmlNode VerhaalIDNode = Data.SelectSingleNode(".//VerhaalID");
                if (VerhaalIDNode != null && VerhaalIDNode.InnerText != "")
                {
                    if (Int64.TryParse(VerhaalIDNode.InnerText, out long VerhaalID))
                    {
                        ProductionMetadata.VerhaalID = VerhaalID;
                    }
                    else
                    {
                        Logger.WriteConsoleAndLog(MsgType.ERROR, "ProductionDataNews", $"Error converting VerhaalID. value={VerhaalIDNode.InnerText} ");
                    }
                }




                string BronList = Data.SelectSingleNode(".//Bron")?.InnerText;
                if (!string.IsNullOrWhiteSpace(BronList))
                {
                    ProductionMetadata.Bron = BronList.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                }

                ProductionMetadata.Wisbeleid = Data.SelectSingleNode("./Wisbeleid")?.InnerText.Trim();
                ProductionMetadata.Memo = Data.SelectSingleNode("./Memo")?.InnerText.Trim();

                return ProductionMetadata;
            }
            return null;
        }


        static private InosMetadata ParseInosMetadata(XmlElement RootElement)
        {
            InosMetadata iNOSMetadata = new InosMetadata();
            XmlNode Data = RootElement.SelectSingleNode("/Asset/BusinessMetadata/Metadata/Type[@name = 'Clip Details']/../Data/ClipDetails");
            if ( Data != null)
            {
                try
                {
                    iNOSMetadata.gewassen = Data.SelectSingleNode(".//gewassen")?.InnerText;

                    iNOSMetadata.VerhaalNaam = Data.SelectSingleNode(".//Verhaalnaam")?.InnerText.Trim();

                    XmlNode VerhaalIDNode = Data.SelectSingleNode(".//VerhaalID");
                    if (VerhaalIDNode != null && VerhaalIDNode.InnerText != "")
                        iNOSMetadata.VerhaalID = Convert.ToInt32(Data.SelectSingleNode(".//VerhaalID").InnerText);

                    XmlNode iNOSAssetIDNode = Data.SelectSingleNode(".//iNOSAssetID");
                    if (iNOSAssetIDNode != null && iNOSAssetIDNode.InnerText != "")
                        iNOSMetadata.InosAssetID = Convert.ToInt32(Data.SelectSingleNode(".//iNOSAssetID").InnerText);

                    iNOSMetadata.ItemNaam = Data.SelectSingleNode(".//Itemnaam")?.InnerText.Trim();

                    XmlNode ItemIDNode = Data.SelectSingleNode(".//ItemID");
                    if (ItemIDNode != null && ItemIDNode.InnerText != "")
                        iNOSMetadata.ItemID = Convert.ToInt32(Data.SelectSingleNode(".//ItemID").InnerText);

                    XmlNode TaakIDNode = Data.SelectSingleNode(".//TaakID");
                    if (TaakIDNode != null && TaakIDNode.InnerText != "")
                        iNOSMetadata.TaakID = Convert.ToInt32(Data.SelectSingleNode(".//TaakID").InnerText);

                    XmlNode iNOSverhaaldatumNode = Data.SelectSingleNode(".//iNOSverhaaldatum");
                    if (iNOSverhaaldatumNode != null && iNOSverhaaldatumNode.InnerText != "")
                        iNOSMetadata.InosVerhaalDatum = Convert.ToDateTime(Data.SelectSingleNode(".//iNOSverhaaldatum").InnerText);

                    XmlNode iNOSverhaalTypeNode = Data.SelectSingleNode(".//iNOSverhaaltype");
                    if (iNOSverhaalTypeNode != null && iNOSverhaalTypeNode.InnerText != "")
                    {
                        Enum.TryParse(iNOSverhaalTypeNode.InnerText, true, out InosVerhaalType myStatus);
                        iNOSMetadata.VerhaalType = myStatus;
                    }

                    iNOSMetadata.Bron = Data.SelectSingleNode(".//Bron")?.InnerText.Trim();

                    return iNOSMetadata;
                }
                catch(Exception ex)
                {
                    Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Error Parsing iNOS metadata {ex.Message}");
                    return null;
                }
            }
            return null;
        }

        static private List<Asset> ParseIds(string ResponseXML)
        {
            List<Asset> AssetList = new List<Asset>();
            // Remove all namespace references, Parsing an XMLDocument with namespaces is HELL.
            var element = XElement.Parse(ResponseXML);
            string NoNamespaceXML = RemoveAllNamespaces(element).ToString();

            XmlElement Root;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(NoNamespaceXML);
                Root = doc.DocumentElement;

                XmlNodeList AssetNodes = Root.SelectNodes(".//Asset");
                foreach(XmlNode Node in AssetNodes)
                {
                    Asset ITXAsset = new Asset();
                    string Uid = Node.Attributes["Uid"].InnerText.Replace(_locationHLVPrefix, "");
                    ITXAsset.AssetID = Convert.ToInt64(Uid);
                    AssetList.Add(ITXAsset);
                }
               
            }
            catch (XmlException ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", "Error loading XML document: " + ex.Message);
            }

            return AssetList;
        }

        // <Asset Result="1" Uid="ITX_HILVERSUM_1_2149772"

        static private string ModifySearchXML(string XML, DateTime FromDate)
        {
            // Remove all namespace references, Parsing an XMLDocument with namespaces is HELL.
            var element = XElement.Parse(XML);
            string NoNamespaceXML = RemoveAllNamespaces(element).ToString();

            XmlElement Root;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(NoNamespaceXML);
                Root = doc.DocumentElement;

                // we need to change the date value
                XmlNode DateTimeModified = Root.SelectSingleNode("//DateTimeModified");
                XmlAttribute Value = DateTimeModified.Attributes["Value"];
                Value.InnerText = FromDate.ToString("yyyy-MM-ddTHH:mm:ss");

                return Root.OuterXml;

            }
            catch (XmlException ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", "Error loading XML document: " + ex.Message);
                return null;
            }
        }



        public static XElement RemoveAllNamespaces(XElement e)
        {
            return new XElement(e.Name.LocalName,
              (from n in e.Nodes()
               select ((n is XElement) ? RemoveAllNamespaces(n as XElement) : n)),
                  (e.HasAttributes) ?
                    (from a in e.Attributes()
                     where (!a.IsNamespaceDeclaration)
                     select new XAttribute(a.Name.LocalName, a.Value)) : null);
        }
    }
}
