﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngestAssetIntoAvid
{
    public class MDProcess
    {
        public long ItxAssetID { get; set; }
        public string ItxAssetName { get; set; }
        public string HostName { get; set; }
        public string Url { get; set; }
        public string State { get; set; }
        public string Group { get; set; }
        public string Percent { get; set; }
        public string Summary { get; set; }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime? StartIngest { get; set; }
        public string MobId { get; set; }
    }
    class LocalObjects
    {
    }
}
