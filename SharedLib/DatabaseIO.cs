﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace SharedLib
{
    static public class DatabaseIO
    {
        // see: http://www.voidcn.com/article/p-phfoefri-bpr.html
          static readonly private string _MyConnString = "server=localhost;uid=itbo;pwd=diwemww11;database=inosmam_migratie;UseAffectedRows=True;SslMode=Required";
      //  static readonly private string _MyConnString = "server=vs-heditmigratie1.edit.nos.nl;uid=itbo;pwd=diwemww11;database=inosmam_migratie;UseAffectedRows=True;SslMode=Required";

        static private bool Open(MySqlConnection Connection)
        {
            try
            {
                Connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"Open Mysql connection: {ex.Message}");
                return false;
            }
        }

        static public List<Asset> GetAssets( bool DeletedAssets = false, string location = "hlv", ITXLocation itxLocation = ITXLocation.hlv)
        {
            string Query = "SELECT * FROM assets WHERE asset_deleted = @asset_deleted and itx_location = @itx_location";

            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return null;

                using (var sc = new MySqlCommand(Query, sq))
                {
                    sc.Parameters.AddWithValue("@asset_deleted", DeletedAssets);
                    sc.Parameters.AddWithValue("@itx_location", location);

                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        List<Asset> AssetList = new List<Asset>();
                        while (mySQlReader.Read())
                        {
                            Asset asset = new Asset() { Assetdata = new AssetData() };
                            asset.AssetID = Convert.ToInt64(mySQlReader["itx_asset_id"]);
                            asset.ITXLocation = (ITXLocation) Enum.Parse(typeof(ITXLocation), mySQlReader.GetString("itx_location"));
                            asset.Assetdata.AssetID = asset.AssetID;
                            asset.Assetdata.ProductionDate = Convert.ToDateTime(mySQlReader["date_produced"]);
                            asset.Assetdata.ContentType = mySQlReader.GetString("content_type");
                            asset.Assetdata.Inpoint = mySQlReader.GetString("inpoint");
                            asset.Assetdata.Duration = mySQlReader.GetString("duration");
                            asset.AssetDeleted = Convert.ToBoolean(mySQlReader["asset_deleted"]);
                            asset.Assetdata.IsPlaceholder = Convert.ToBoolean(mySQlReader["is_placeholder"]);
                            asset.Assetdata.LastModified = Convert.ToDateTime(mySQlReader["date_last_modified"]);
                            asset.Assetdata.AssetName = mySQlReader.GetString("guci");
                            asset.Assetdata.AssetTitle = mySQlReader.GetString("title");
                            asset.Assetdata.InosMetadata = JsonConvert.DeserializeObject<InosMetadata>(mySQlReader.GetString("inos_metadata"));
                            asset.Assetdata.ProductionMetadataNews = JsonConvert.DeserializeObject<ProductionMetataNews>(mySQlReader.GetString("production_metadata_news"));
                            asset.Assetdata.ProductionMetadataSport = JsonConvert.DeserializeObject<ProductionMetadataSport>(mySQlReader.GetString("production_metadata_sport"));
                            asset.Assetdata.ArchiveMetadataNews = JsonConvert.DeserializeObject<ArchiveMetaDataNews>(mySQlReader.GetString("archive_metadata_news"));
                            asset.Assetdata.ArchiveMetaDataSport = JsonConvert.DeserializeObject<ArchiveMetaDataSport>(mySQlReader.GetString("archive_metadata_sport"));
                            asset.Assetdata.Logevents = JsonConvert.DeserializeObject<List<LoggingMetadata>>(mySQlReader.GetString("logging_metadata")).ToArray();
                            if (Convert.IsDBNull(mySQlReader[$"interplay_mobid_{location}"]) == false)
                                asset.InterplayMobID = mySQlReader.GetString($"interplay_mobid_{location}");
                            if (Convert.IsDBNull(mySQlReader[$"ingest_status_{location}"]) == false)
                                asset.IngestStatus = mySQlReader.GetString($"ingest_status_{location}");
                            if (Convert.IsDBNull(mySQlReader[$"interplay_size_{location}"]) == false)
                                asset.InterplaysizeBytes = Convert.ToInt64( mySQlReader[$"interplay_size_{location}"]);
                            AssetList.Add(asset);
                        }
                        return AssetList;
                    }
                }
            }
        }

        static public int UpdateAsset(Asset ITXAsset)
        {
            // if ingestlocation is not set than its an update from the InsertTo-DB action
            if (ITXAsset.IngestLocation == null) ITXAsset.IngestLocation = "hlv";
            string Query = @$"UPDATE assets SET 
                                guci = COALESCE(@guci, guci),       
                                title = COALESCE(@title, title),
                                date_produced = COALESCE(@date_produced, date_produced),
                                inpoint = COALESCE(@inpoint, inpoint),
                                duration = COALESCE(@duration, duration),
                                content_type = COALESCE(@content_type, content_type),
                                categories = COALESCE(@categories, categories),
                                is_placeholder = COALESCE(@is_placeholder, is_placeholder),
                                notes = COALESCE(@notes, notes),
                                inos_metadata = COALESCE(@inos_metadata, inos_metadata),
                                production_metadata_news = COALESCE(@production_metadata_news, production_metadata_news),
                                production_metadata_sport = COALESCE(@production_metadata_sport, production_metadata_sport),
                                archive_metadata_news = COALESCE(@archive_metadata_news, archive_metadata_news),
                                archive_metadata_sport = COALESCE(@archive_metadata_sport, archive_metadata_sport),
                                logging_metadata = COALESCE(@logging_metadata, logging_metadata),
                                file_location = COALESCE(@file_location, file_location),
                                date_created = COALESCE(@date_created, date_created),
                                date_last_modified = COALESCE(@date_last_modified, date_last_modified),
                                asset_deleted = COALESCE(@asset_deleted, asset_deleted),
                                file_media_info = COALESCE(@file_media_info, file_media_info),
                                ingest_status_{ITXAsset.IngestLocation} = COALESCE(@ingest_status, ingest_status_{ITXAsset.IngestLocation}),
                                ingest_started_{ITXAsset.IngestLocation} = COALESCE(@ingest_started, ingest_started_{ITXAsset.IngestLocation}),
                                ingest_message_{ITXAsset.IngestLocation} = COALESCE(@ingest_message, ingest_message_{ITXAsset.IngestLocation}),
                                ingest_progress_{ITXAsset.IngestLocation} = COALESCE(@ingest_progress, ingest_progress_{ITXAsset.IngestLocation}),
                                ingest_error_message_{ITXAsset.IngestLocation} = COALESCE(@ingest_error_message, ingest_error_message_{ITXAsset.IngestLocation}),
                                interplay_mobid_{ITXAsset.IngestLocation} = COALESCE(@interplay_mobid, interplay_mobid_{ITXAsset.IngestLocation}),
                                error_description = IF(ISnull(error_description),@error_description,CONCAT_WS('\r\n',error_description,@error_description)),
                                record_updated = @record_updated,
                                interplay_locators_{ITXAsset.IngestLocation} = COALESCE(@interplay_locators, interplay_locators_{ITXAsset.IngestLocation}),
                                interplay_size_{ITXAsset.IngestLocation} = COALESCE(@interplay_size, interplay_size_{ITXAsset.IngestLocation})
                             WHERE itx_asset_id = @itx_asset_id";

            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return 0;

                using (var sc = new MySqlCommand(Query, sq))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@guci", ITXAsset.Assetdata.AssetName);
                        sc.Parameters.AddWithValue("@itx_asset_id", ITXAsset.AssetID);
                        sc.Parameters.AddWithValue("@title", ITXAsset.Assetdata.AssetTitle);
                        sc.Parameters.AddWithValue("@date_produced", ITXAsset.Assetdata.ProductionDate);
                        sc.Parameters.AddWithValue("@inpoint", ITXAsset.Assetdata.Inpoint);
                        sc.Parameters.AddWithValue("@duration", ITXAsset.Assetdata.Duration);
                        sc.Parameters.AddWithValue("@content_type", ITXAsset.Assetdata.ContentType);
                        sc.Parameters.AddWithValue("@is_placeholder", ITXAsset.Assetdata.IsPlaceholder);
                        sc.Parameters.AddWithValue("@interplay_size", ITXAsset.InterplaysizeBytes);

                        string CategoriesVar = null;
                        if(ITXAsset.Assetdata.Categories != null)
                        {
                            CategoriesVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.Categories);
                        }
                        sc.Parameters.AddWithValue("@categories", CategoriesVar);
                        sc.Parameters.AddWithValue("@notes", ITXAsset.Assetdata.Notes);

                        string InosMetadataVar = null;
                        if (ITXAsset.Assetdata.InosMetadata != null)
                        {
                            InosMetadataVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.InosMetadata, new Newtonsoft.Json.Converters.StringEnumConverter());
                        }
                        sc.Parameters.AddWithValue("@inos_metadata", InosMetadataVar);
                        
                        string ProductionMetadataNewsVar = null;
                        if (ITXAsset.Assetdata.ProductionMetadataNews != null)
                        {
                            ProductionMetadataNewsVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.ProductionMetadataNews, new Newtonsoft.Json.Converters.StringEnumConverter());
                        }
                        sc.Parameters.AddWithValue("@production_metadata_news", ProductionMetadataNewsVar);

                        string ProductionMetadataSportVar = null;
                        if (ITXAsset.Assetdata.ProductionMetadataSport != null)
                        {
                            ProductionMetadataSportVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.ProductionMetadataSport, new Newtonsoft.Json.Converters.StringEnumConverter());
                        }
                        sc.Parameters.AddWithValue("@production_metadata_sport", ProductionMetadataSportVar);

                        string ArchiveMetadataNewsVar = null;
                        if (ITXAsset.Assetdata.ArchiveMetadataNews != null)
                        {
                            ArchiveMetadataNewsVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.ArchiveMetadataNews, new Newtonsoft.Json.Converters.StringEnumConverter());
                        }
                        sc.Parameters.AddWithValue("@archive_metadata_news", ArchiveMetadataNewsVar);

                        string ArchiveMetaDataSportVar = null;
                        if (ITXAsset.Assetdata.ArchiveMetaDataSport != null)
                        {
                            ArchiveMetaDataSportVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.ArchiveMetaDataSport, new Newtonsoft.Json.Converters.StringEnumConverter());
                        }
                        sc.Parameters.AddWithValue("@archive_metadata_sport", ArchiveMetaDataSportVar);

                        string LoggingEventsVar = null;
                        if (ITXAsset.Assetdata.Logevents != null)
                        {
                            LoggingEventsVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.Logevents, new Newtonsoft.Json.Converters.StringEnumConverter());
                        }
                        sc.Parameters.AddWithValue("@logging_metadata", LoggingEventsVar);

                        string FileMediaInfo = null;
                        if(ITXAsset.FileMediaInfo != null)
                        {
                            FileMediaInfo = JsonConvert.SerializeObject(ITXAsset.FileMediaInfo, Formatting.Indented);
                        }
                        sc.Parameters.AddWithValue("@file_media_info", FileMediaInfo);
                        sc.Parameters.AddWithValue("@file_location", ITXAsset.Assetdata.MediaLocation);
                        sc.Parameters.AddWithValue("@date_created", ITXAsset.Assetdata.Created);
                        sc.Parameters.AddWithValue("@date_last_modified", ITXAsset.Assetdata.LastModified);
                        sc.Parameters.AddWithValue("@asset_deleted", ITXAsset.AssetDeleted);
                        sc.Parameters.AddWithValue("@ingest_status", ITXAsset.IngestStatus);
                        sc.Parameters.AddWithValue("@ingest_started", ITXAsset.IngestStarted);
                        sc.Parameters.AddWithValue("@ingest_message", ITXAsset.IngestMessage);
                        sc.Parameters.AddWithValue("@ingest_progress", ITXAsset.IngestProgress);
                        sc.Parameters.AddWithValue("@ingest_error_message", ITXAsset.IngestErrorMessage);
                        sc.Parameters.AddWithValue("@error_description", ITXAsset.ErrorDescription);
                        sc.Parameters.AddWithValue("@interplay_mobid", ITXAsset.InterplayMobID);
                        sc.Parameters.AddWithValue("@record_updated", DateTime.UtcNow);

                        string LocatorVar = null;
                        if (ITXAsset.Assetdata.AvidLocators != null)
                        {
                            LocatorVar = JsonConvert.SerializeObject(ITXAsset.Assetdata.AvidLocators, new Newtonsoft.Json.Converters.StringEnumConverter());
                        }
                        sc.Parameters.AddWithValue("@interplay_locators", LocatorVar);

                        int Updated = sc.ExecuteNonQuery();   // number of rows affected by 
                        Logger.WriteConsoleAndLog(MsgType.INFO, ITXAsset.AssetID.ToString(), $"Asset updated:{Updated.ToString()} Name:{ITXAsset.Assetdata.AssetName}");
                        return Updated;
                    }
                    catch (MySqlException ex)
                    {
                        var ErrorMessage = $"Message: {ex.Message} InnerException1: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, ITXAsset.AssetID.ToString(), $"UpdateAsset1: {ITXAsset.Assetdata.AssetName} {ErrorMessage}");
                        return 0;
                    }
                    catch (NullReferenceException ex)
                    {
                        var ErrorMessage = $"Message: {ex.Message} InnerException2: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, ITXAsset.AssetID.ToString(), $"UpdateAsset2: {ITXAsset.Assetdata.AssetName} {ErrorMessage}");
                        return 0;
                    }
                    catch (AggregateException ex)
                    {
                        var ErrorMessage = $"Message: {ex.Message} InnerException2: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, ITXAsset.AssetID.ToString(), $"UpdateAsset3: {ITXAsset.Assetdata.AssetName} {ErrorMessage}");
                        return 0;
                    }
                    catch (Exception ex)
                    {
                        // Added a final exception in case the above does not catch the nullreference exception.
                        var ErrorMessage = $"Message: {ex.Message} InnerException3: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, ITXAsset.AssetID.ToString(), $"UpdateAsset4: {ITXAsset.Assetdata.AssetName} {ErrorMessage}");
                        return 0;
                    }
                }
            }
        }


        static public void InsertAsset(Asset ITXAsset)
        {
            string intmobID = $"interplay_mobid_{ITXAsset.ITXLocation}";

            string sqlString = $@"INSERT INTO assets (   itx_asset_id,
                                                        itx_location,
                                                        guci,
                                                        title,
                                                        date_produced,
                                                        inpoint,
                                                        duration,
                                                        is_placeholder,
                                                        content_type,
                                                        categories,
                                                        notes,
                                                        inos_metadata,
                                                        production_metadata_news,
                                                        production_metadata_sport,
                                                        archive_metadata_news,
                                                        archive_metadata_sport,
                                                        logging_metadata,
                                                        file_location,
                                                        date_created,
                                                        date_last_modified,
                                                        file_media_info,
                                                        asset_deleted,
                                                        record_created,
                                                        error_description,
                                                        {intmobID}
                               ) VALUES ( 
                                                        @itx_asset_id,
                                                        @itx_location,
                                                        @guci,
                                                        @title,
                                                        @date_produced,
                                                        @inpoint,
                                                        @duration,
                                                        @is_placeholder,
                                                        @content_type,
                                                        @categories,
                                                        @notes,
                                                        @inos_metadata,
                                                        @production_metadata_news,
                                                        @production_metadata_sport,
                                                        @archive_metadata_news,
                                                        @archive_metadata_sport,
                                                        @logging_metadata,
                                                        @file_location,
                                                        @date_created,
                                                        @date_last_modified,
                                                        @file_media_info,
                                                        @asset_deleted,
                                                        @record_created,
                                                        @error_description,
                                                        @{intmobID}
                              )";

            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return;

                using (var sc = new MySqlCommand(sqlString, sq))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@itx_asset_id", ITXAsset.AssetID);
                        sc.Parameters.AddWithValue("@itx_location", ITXAsset.ITXLocation.ToString());
                        sc.Parameters.AddWithValue("@guci", ITXAsset.Assetdata.AssetName);
                        sc.Parameters.AddWithValue("@title", ITXAsset.Assetdata.AssetTitle);
                        sc.Parameters.AddWithValue("@date_produced", ITXAsset.Assetdata.ProductionDate);
                        sc.Parameters.AddWithValue("@inpoint", ITXAsset.Assetdata.Inpoint);
                        sc.Parameters.AddWithValue("@duration", ITXAsset.Assetdata.Duration);
                        sc.Parameters.AddWithValue("@file_umid", ITXAsset.FileUMID); // store as bytes ??
                        sc.Parameters.AddWithValue("@is_placeholder", ITXAsset.Assetdata.IsPlaceholder); // store as bytes ??
                        sc.Parameters.AddWithValue("@content_type", ITXAsset.Assetdata.ContentType);
                        sc.Parameters.AddWithValue("@categories", JsonConvert.SerializeObject(ITXAsset.Assetdata.Categories,Formatting.Indented));
                        sc.Parameters.AddWithValue("@notes", ITXAsset.Assetdata.Notes);
                        sc.Parameters.AddWithValue("@inos_metadata", JsonConvert.SerializeObject(ITXAsset.Assetdata.InosMetadata,Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter()));
                        sc.Parameters.AddWithValue("@production_metadata_news", JsonConvert.SerializeObject(ITXAsset.Assetdata.ProductionMetadataNews,Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter()));
                        sc.Parameters.AddWithValue("@production_metadata_sport", JsonConvert.SerializeObject(ITXAsset.Assetdata.ProductionMetadataSport,Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter()));
                        sc.Parameters.AddWithValue("@archive_metadata_news", JsonConvert.SerializeObject(ITXAsset.Assetdata.ArchiveMetadataNews,Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter()));
                        sc.Parameters.AddWithValue("@archive_metadata_sport", JsonConvert.SerializeObject(ITXAsset.Assetdata.ArchiveMetaDataSport,Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter()));
                        sc.Parameters.AddWithValue("@logging_metadata", JsonConvert.SerializeObject(ITXAsset.Assetdata.Logevents,Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter()));
                        sc.Parameters.AddWithValue("@file_location", ITXAsset.Assetdata.MediaLocation);
                        sc.Parameters.AddWithValue("@date_created", ITXAsset.Assetdata.Created);
                        sc.Parameters.AddWithValue("@date_last_modified", ITXAsset.Assetdata.LastModified);
                        sc.Parameters.AddWithValue("@file_media_info", JsonConvert.SerializeObject(ITXAsset.FileMediaInfo,Formatting.Indented, new Newtonsoft.Json.Converters.StringEnumConverter()));
                        sc.Parameters.AddWithValue("@asset_deleted", false);
                        sc.Parameters.AddWithValue("@record_created", DateTime.UtcNow);
                        sc.Parameters.AddWithValue("@error_description", ITXAsset.ErrorDescription);
                        sc.Parameters.AddWithValue($"@{intmobID}", ITXAsset.InterplayMobID);
                        sc.ExecuteNonQuery();
                        Logger.WriteConsoleAndLog(MsgType.INFO, ITXAsset.AssetID.ToString(), $"Asset added: {ITXAsset.Assetdata.AssetName}");
                    }
                    catch (MySqlException ex)
                    {
                        Logger.WriteConsoleAndLog(MsgType.ERROR, "", $"InsertAsset: Code:{ex.ErrorCode}  Message:{ex.Message} InnerMessage:{ex.InnerException}");
                    }
                }
            }
        }



        static public List<Asset> GetRunningJobsHlv()
        {
            string Query = @"SELECT
                                 *
                             FROM
	                             `assets` 
                             WHERE
                                 ingest_guid_hlv IS NOT NULL
                                 AND transfer_succes_hlv IS NULL
                                 AND asset_deleted = false";

            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return null;

                using (var sc = new MySqlCommand(Query, sq))
                {
                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        List<Asset> AssetList = new List<Asset>();
                        while (mySQlReader.Read())
                        {
                            Asset asset = new Asset();
                            asset.AssetID = Convert.ToInt64(mySQlReader["itx_asset_id"]);
                            if (Convert.IsDBNull(mySQlReader["transfer_started_hlv"]) == false)
                                asset.IngestStarted = Convert.ToDateTime(mySQlReader["transfer_started_hlv"]);
                            AssetList.Add(asset);
                        }
                        return AssetList;
                    }
                }
            }
        }


        static public Asset GetAssetToTransfer(string IngestLocation)
        {
            Guid ingest_guid = Guid.NewGuid();

            // ITX's Autoarchive moves clips from MAM_MEDIA to MAM_ARCHIVE_MEDIA evey 2 hrs.
            // The export file is made every 6 hrs.
            // So select only older clips.

            // Sport ingest hilversum exception
            string SportIngests;
            if (IngestLocation.ToLower() == "hlv")
                SportIngests = "OR content_type='CSI' OR content_type='CST'";
            else
                SportIngests = "";

          //  SportIngests = ""; // --> for now skip the sport ingests.

            string UpdateQuery = @$"UPDATE assets 
                                  SET ingest_guid_{IngestLocation} = @ingest_guid 
                                  WHERE
	                                  ingest_guid_{IngestLocation} is NULL
                                      AND date_last_modified < NOW( ) - INTERVAL 12 HOUR
                                      AND asset_deleted = 0
                                      AND is_placeholder = 0
                                      AND ( 
                                               SUBSTR(content_type,3,1) = 'G'
                                            OR SUBSTR(content_type,3,1) = 'A' 
                                            OR SUBSTR(content_type,3,1) = 'K'
                                            OR SUBSTR(content_type,3,1) = 'D'
                                            OR SUBSTR(content_type,3,1) = 'xV'
                                            { SportIngests}
                                          )
                                      ORDER BY date_created desc
	                              LIMIT 1";

            string SelectQuery = @$"SELECT 
                                        * 
                                     FROM 
                                        assets
                                     WHERE ingest_guid_{IngestLocation} = @ingest_guid";


            using (MySqlConnection sq = new MySqlConnection(_MyConnString))
            {
                if (Open(sq) == false) return null;

                using (var sc = new MySqlCommand(UpdateQuery, sq))
                {
                    try
                    {
                        sc.Parameters.AddWithValue("@ingest_guid", ingest_guid);
                        int Updated = sc.ExecuteNonQuery();   // 1 = new / 2 = update / 0 = not changed
                        if (Updated == 0) return null;  // noting to do
                    }
                    catch (MySqlException ex)
                    {
                        string ErrorMessage = $"Message: {ex.Message} InnerException: {ex.InnerException}";
                        Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to set guid: {ErrorMessage}");
                        return null;
                    }
                }


                using (var sc = new MySqlCommand(SelectQuery, sq))
                {
                    sc.Parameters.AddWithValue($"@ingest_guid", ingest_guid);
                    using (MySqlDataReader mySQlReader = sc.ExecuteReader())
                    {
                        try
                        {
                            Asset asset = new Asset() {  IngestLocation = IngestLocation,   Assetdata = new AssetData() };
                            while (mySQlReader.Read())
                            {
                                asset.AssetID = Convert.ToInt64(mySQlReader["itx_asset_id"]);
                                asset.Assetdata.AssetID = asset.AssetID;
                                asset.Assetdata.AssetName = mySQlReader.GetString("guci");
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("title")))
                                    asset.Assetdata.AssetTitle = mySQlReader.GetString("title");
                                asset.Assetdata.ProductionDate = Convert.ToDateTime(mySQlReader["date_produced"]);
                                asset.Assetdata.Inpoint = mySQlReader.GetString("inpoint");
                                asset.Assetdata.Duration = mySQlReader.GetString("duration");
                                asset.Assetdata.IsPlaceholder = Convert.ToBoolean(mySQlReader["is_placeholder"]);
                                asset.Assetdata.ContentType = mySQlReader.GetString("content_type");
                                asset.Assetdata.Categories = JsonConvert.DeserializeObject<string[]>(mySQlReader.GetString("categories"));
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("notes")))
                                    asset.Assetdata.Notes = mySQlReader.GetString("notes");
                                asset.Assetdata.InosMetadata = JsonConvert.DeserializeObject<InosMetadata>(mySQlReader.GetString("inos_metadata"));
                                asset.Assetdata.ProductionMetadataNews = JsonConvert.DeserializeObject<ProductionMetataNews>(mySQlReader.GetString("production_metadata_news"));
                                asset.Assetdata.ProductionMetadataSport = JsonConvert.DeserializeObject<ProductionMetadataSport>(mySQlReader.GetString("production_metadata_sport"));
                                asset.Assetdata.ArchiveMetadataNews = JsonConvert.DeserializeObject<ArchiveMetaDataNews>(mySQlReader.GetString("archive_metadata_news"));
                                asset.Assetdata.ArchiveMetaDataSport = JsonConvert.DeserializeObject<ArchiveMetaDataSport>(mySQlReader.GetString("archive_metadata_sport"));
                                asset.Assetdata.Logevents = JsonConvert.DeserializeObject<List<LoggingMetadata>>(mySQlReader.GetString("logging_metadata")).ToArray();
                                if (!mySQlReader.IsDBNull(mySQlReader.GetOrdinal("file_location")))
                                    asset.Assetdata.MediaLocation = mySQlReader.GetString("file_location");
                                asset.Assetdata.Created = Convert.ToDateTime(mySQlReader.GetString("date_created"));
                                asset.Assetdata.LastModified = Convert.ToDateTime(mySQlReader.GetString("date_last_modified"));
                                asset.FileMediaInfo = JsonConvert.DeserializeObject<MediaInfo>(mySQlReader.GetString("file_media_info"));
                            }
                            return asset;
                        }
                        catch (MySqlException ex)
                        {
                            string ErrorMessage = $"Message1: {ex.Message} InnerException: {ex.InnerException}";
                            Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to get asset: {ErrorMessage}");
                            return null;
                        }
                        catch (NullReferenceException ex)
                        {
                            string ErrorMessage = $"Message2: {ex.Message} InnerException: {ex.InnerException}";
                            Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to get asset: {ErrorMessage}");
                            return null;
                        }
                        catch (Exception ex)
                        {
                            string ErrorMessage = $"Message3: {ex.Message} InnerException: {ex.InnerException}";
                            Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to get asset: {ErrorMessage}");
                            return null;
                        }
                    }
                }
            }
        }
    }
}
