﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Newtonsoft.Json;
using ServiceReference1;
namespace SharedLib
{
    public static class InterplayService
    {
        static public string _interplayUri = "interplay://IP_WS_WG/";
        static private UserCredentialsType _user;
        static private AssetsPortTypeClient _client;

        static public void Init(string Location)
        {
            string Url;
            if (Location == "hlv")
            {
                Url = "http://vs-heditws2.edit.nos.nl/services/Assets";
            }
            else if (Location == "dhg")
            {
                Url = "http://vs-deditws1.edit.nos.nl/services/Assets";
            }
            else if (Location == "tst")
            {
                Url = "http://vs-heditws-tst.edit.nos.nl/services/Assets";
            }
            else
            {
                return;
            }
            _user = new UserCredentialsType() { Username = "Administrator", Password = "NOS-edit" };

            //    _client = new AssetsPortTypeClient(AssetsPortTypeClient.EndpointConfiguration.AssetsPort );
            _client = new AssetsPortTypeClient(new BasicHttpBinding() { MaxReceivedMessageSize = 550000000, MaxBufferSize = 550000000, ReceiveTimeout = TimeSpan.FromMinutes(40), SendTimeout = TimeSpan.FromMinutes(40) },
                new EndpointAddress(Url)
                 );

            //new BasicHttpBinding() { MaxReceivedMessageSize = 20000000, MaxBufferSize = 20000000, ReceiveTimeout = TimeSpan.FromMinutes(5), SendTimeout = TimeSpan.FromMinutes(5)  },
            //new EndpointAddress(Url));
            //         var channel = _client.ChannelFactory.CreateChannel();
        }

        static public List<Asset> GetArchiveClips2(string interplayFolderURI, List<Asset> InterplayAssetList)
        {
            // same as below bt with getchildren method
            AssetDescriptionType[] InterplayAsset = getChildren(interplayFolderURI, true, false, ".*");
            AssetDescriptionType[] InterplayFolderAssets = InterplayAsset.Where(x => x.Attributes.FirstOrDefault(y => y.Name == "Type").Value == "folder").ToArray();
            AssetDescriptionType[] InterplaySortedFolderAssets = InterplayFolderAssets.OrderBy(c => c.InterplayURI).ToArray();
            AssetDescriptionType[] InterplayMasterClipAssets = InterplayAsset.Where(x => x.Attributes.FirstOrDefault(y => y.Name == "Type").Value == "masterclip").ToArray();


            foreach (AssetDescriptionType assetdescription in InterplayMasterClipAssets)
            {
                Asset asset = new Asset() { Assetdata = new AssetData() };
                asset.InterplayMobID = assetdescription.InterplayURI;
                asset.Assetdata.AssetName = assetdescription.Attributes.FirstOrDefault(x => x.Name == "Display Name").Value;
                asset.InterplayPath = assetdescription.Attributes.FirstOrDefault(x => x.Name == "Path").Value;
                asset.Assetdata.AssetTitle = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Titel")?.Value ?? "";
                string VideoIDStr = assetdescription.Attributes.FirstOrDefault(x => x.Name == "Video ID")?.Value;
                if (Int64.TryParse(VideoIDStr, out long VideoID))
                {
                    asset.Assetdata.AssetID = VideoID;
                    asset.AssetID = asset.Assetdata.AssetID;
                }
                else
                {
                    // asset is not an archive assets
                    continue;
                }
                asset.Assetdata.ContentType = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS_Clip_Content_Type").Value;

                string iNOS_Housekeeping = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Housekeeping")?.Value;
                if (iNOS_Housekeeping == "T-0")
                {
                    asset.AssetDeleted = true;
                }


                string iNOS_Verhaaldatum = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Verhaaldatum")?.Value;
                if (DateTime.TryParse(iNOS_Verhaaldatum, out DateTime VerhaalDatum))
                {
                    asset.Assetdata.InosMetadata = new InosMetadata() { InosVerhaalDatum = VerhaalDatum };
                }

                string iNOS_Vervaldatum = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Vervaldatum")?.Value;
                if (DateTime.TryParse(iNOS_Vervaldatum, out DateTime VervalDatum))
                {
                    asset.Assetdata.ArchiveMetaDataSport = new ArchiveMetaDataSport() { VervalDatum = VervalDatum };
                    asset.Assetdata.ArchiveMetadataNews = new ArchiveMetaDataNews() { VervalDatum = VervalDatum };
                }

                InterplayAssetList.Add(asset);
            }


            foreach (AssetDescriptionType Asset in InterplaySortedFolderAssets)
            {
                InterplayAssetList = GetArchiveClips2(Asset.InterplayURI, InterplayAssetList);
            }
            Logger.WriteConsoleAndLog(MsgType.INFO, null, $"InterplayFolder: {interplayFolderURI} assets:{InterplayAsset.Count()} total-assets:{InterplayAssetList.Count()}");
            return InterplayAssetList;
        }

        static public List<Asset> GetArchiveClips(string interplayFolder)
        {
            AssetDescriptionType[] searchResults = GetMasterClips(_interplayUri, interplayFolder);

            List<Asset> InterplayAssetList = new List<Asset>();

            foreach (AssetDescriptionType assetdescription in searchResults)
            {
                Asset asset = new Asset() { Assetdata = new AssetData() };
                asset.InterplayMobID = assetdescription.InterplayURI;
                asset.Assetdata.AssetName = assetdescription.Attributes.FirstOrDefault(x => x.Name == "Display Name").Value;
                asset.InterplayPath = assetdescription.Attributes.FirstOrDefault(x => x.Name == "Path").Value;
                asset.Assetdata.AssetTitle = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Titel")?.Value ?? "";
                string VideoIDStr = assetdescription.Attributes.FirstOrDefault(x => x.Name == "Video ID")?.Value;
                if (Int64.TryParse(VideoIDStr, out long VideoID))
                {
                    asset.Assetdata.AssetID = VideoID;
                    asset.AssetID = asset.Assetdata.AssetID;
                }
                else
                {
                    // asset might not be an archive assets
                    continue;
                }
                asset.Assetdata.ContentType = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS_Clip_Content_Type").Value;

                string iNOS_Housekeeping = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Housekeeping")?.Value;
                if (iNOS_Housekeeping == "T-0")
                {
                    asset.AssetDeleted = true;
                }


                string iNOS_Verhaaldatum = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Verhaaldatum")?.Value;
                if (DateTime.TryParse(iNOS_Verhaaldatum, out DateTime VerhaalDatum))
                {
                    asset.Assetdata.InosMetadata = new InosMetadata() { InosVerhaalDatum = VerhaalDatum };
                }

                string iNOS_Vervaldatum = assetdescription.Attributes.FirstOrDefault(x => x.Name == "iNOS Vervaldatum")?.Value;
                if (DateTime.TryParse(iNOS_Vervaldatum, out DateTime VervalDatum))
                {
                    asset.Assetdata.ArchiveMetaDataSport = new ArchiveMetaDataSport() { VervalDatum = VervalDatum };
                    asset.Assetdata.ArchiveMetadataNews = new ArchiveMetaDataNews() { VervalDatum = VervalDatum };
                }

                InterplayAssetList.Add(asset);
            }
            Logger.WriteConsoleAndLog(MsgType.INFO, null, $"Found {InterplayAssetList.Count} archive assets in Interplay");
            return InterplayAssetList;
        }


        static public bool UpdateMultiAttributes(Asset asset)
        {
            // convert rechten EN --> NL
            //int indexval = (int) asset.Assetdata.ArchiveMetaDataSport?.RechtenVrij;
            //string rechtenvrijNL = ((RechtenVrij_NL) indexval).ToString();

            string IsDeleted = asset.AssetDeleted == true ? "T-0" : "";

            string Vervaldatum = "";
            if (asset.Assetdata.ArchiveMetadataNews?.VervalDatum != null)
            {
                Vervaldatum = ((DateTime)asset.Assetdata.ArchiveMetadataNews.VervalDatum).ToString("yyyy-MM-dd");
            }
            else if (asset.Assetdata.ArchiveMetaDataSport?.VervalDatum != null)
            {
                Vervaldatum = ((DateTime)asset.Assetdata.ArchiveMetaDataSport.VervalDatum).ToString("yyyy-MM-dd");
            }

            string InosVerhaalDatum = "";
            if (asset.Assetdata.InosMetadata?.InosVerhaalDatum != null)
            {
                InosVerhaalDatum = ((DateTime)asset.Assetdata.InosMetadata.InosVerhaalDatum).ToString("yyyy-MM-dd");
            }

            // Letop!! attributes mogen max 30 karakters zijn
            var attributes = new SetAttributesType();
            attributes.InterplayURIs = new string[] { "interplay://IP_WS_WG?mobid=" + asset.InterplayMobID };
            attributes.Attributes = new AttributeType[]
            {
                new AttributeType(){ Group="USER", Name="Display Name", Value = asset.Assetdata.AssetName},
                new AttributeType(){ Group="USER", Name="iNOS Titel", Value = asset.Assetdata.AssetTitle},
                new AttributeType(){ Group="USER", Name="iNOS_Clip_Content_Type", Value = asset.Assetdata.ContentType},
                new AttributeType(){ Group="USER", Name="iNOS Vervaldatum", Value = Vervaldatum},
                new AttributeType(){ Group="USER", Name="iNOS Verhaaldatum", Value = InosVerhaalDatum},
     //           new AttributeType(){ Group="USER", Name="iNOS Rechtenvrij", Value = rechtenvrijNL },
                new AttributeType(){ Group="USER", Name="iNOS Housekeeping", Value = IsDeleted},
            };

            var response = _client.SetAttributes(_user, attributes);
            if (response.Errors == null)
            {
                Logger.WriteConsoleAndLog(MsgType.INFO, "---", $"UpdateMultiAttributes: {asset.Assetdata.AssetName}");
            }
            else
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "---", $"UpdateMultiAttributes: {JsonConvert.SerializeObject(response, Formatting.Indented)}");
            }
            return response.Errors == null;
        }




        static private AssetDescriptionType[] getChildren(string interplayBasePath, bool includeFolders, bool includeFiles, string nameRegexFilter)
        {
            var param = new GetChildrenType();
            param.InterplayURI = interplayBasePath;
            param.IncludeFolders = includeFolders;
            param.IncludeFiles = includeFiles;
            param.NameFilter = nameRegexFilter;
            return _client.GetChildren(_user, param).Results;
        }

        static public string NonMigratieMasterClip(string InterplayURI, string InterplayFolder, string assetName)
        {
            SearchGroupType searchgroup = CreateSearchQuery("AND", new List<SearchFilter>(){
                 new SearchFilter(){ Group = "USER", Name = "Display Name", Value = assetName, Condition = "EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Media Status", Value = "online", Condition = "EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Type", Value = "Masterclip", Condition = "EQUALS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Tracks", Value = "V", Condition = "CONTAINS" },
                 new SearchFilter(){ Group = "SYSTEM", Name = "Path", Value = "01 Materiaal", Condition = "CONTAINS" },
                });


            SearchType Search = new SearchType()
            {
                InterplayPathURI = $"{InterplayURI}{InterplayFolder}",
                MaxResults = 50,
                MaxResultsSpecified = true,
                SearchGroup = searchgroup
            };

            SearchResponseType SearchResult = null;
            try
            {
                SearchResult = _client.Search(_user, Search);
                if (SearchResult.Errors == null)
                {
                    // geen resultaat -> null
                    // 1 of meer -> oudste mobid
                    AssetDescriptionType assetDescription = SearchResult.Results.OrderBy(x => x.Attributes.FirstOrDefault(y => y.Name == "Creation Date").Value).FirstOrDefault();
                    if(assetDescription == null)
                    {
                        return null;
                    }
                    else
                    {
                        return assetDescription.Attributes.FirstOrDefault(x => x.Name == "MOB ID").Value;
                    }
                    return "";
                }
                else
                {
                    Logger.WriteConsoleAndLog(MsgType.ERROR, "---", $"Getmasterclps: {JsonConvert.SerializeObject(SearchResult, Formatting.Indented)}");
                    return null;
                }
            }
            catch (Exception ex)  //timeout
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "---", $"Getmasterclps: {ex.Message}");
                return null;
            }


        }

        static public AttributeConditionType CreateConditionType(SearchFilter filter)
        {
            var SearchAttribute1 = new AttributeType() { Group = filter.Group, Name = filter.Name, Value = filter.Value };
            return new AttributeConditionType() { Condition = filter.Condition, Attribute = SearchAttribute1 };
        }

        static public SearchGroupType CreateSearchQuery(string Operator, List<SearchFilter> filters)
        {
            var attributeConditionTypes = new List<AttributeConditionType>();
            foreach (var filter in filters)
                attributeConditionTypes.Add(CreateConditionType(filter));
            var searchGroup = new SearchGroupType() { Operator = Operator, AttributeCondition = attributeConditionTypes.ToArray() };
            return new SearchGroupType() { Operator = "OR", SearchGroup = new List<SearchGroupType>() { searchGroup }.ToArray() };
        }



        static public AssetDescriptionType[] GetMasterClips(string InterplayURI, string InterplayFolder)
        {
            List<SearchGroupType> AndSearches = new List<SearchGroupType>();

            AttributeType SearchAttribute = new AttributeType() { Group = "SYSTEM", Name = "Type", Value = "masterclip" };
            AttributeConditionType AttributeCondition1 = new AttributeConditionType() { Condition = "EQUALS", Attribute = SearchAttribute };

            AttributeConditionType[] AttributeConditionArr = new AttributeConditionType[] { AttributeCondition1 };

            SearchGroupType SearchGroup = new SearchGroupType() { Operator = "AND", AttributeCondition = AttributeConditionArr };
            AndSearches.Add(SearchGroup);

            SearchGroupType OrSearchgroup = new SearchGroupType() { Operator = "OR", SearchGroup = AndSearches.ToArray() };

            SearchType Search = new SearchType()
            {
                InterplayPathURI = $"{InterplayURI}{InterplayFolder}",
                MaxResults = 150000,
                MaxResultsSpecified = true,
                SearchGroup = OrSearchgroup
            };

            SearchResponseType SearchResult = null;
            try
            {
                SearchResult = _client.Search(_user, Search);
                if (SearchResult.Errors == null)
                {
                    return SearchResult.Results;
                }
                else
                {
                    Logger.WriteConsoleAndLog(MsgType.ERROR, "---", $"Getmasterclps: {JsonConvert.SerializeObject(SearchResult, Formatting.Indented)}");
                    return null;
                }
            }
            catch (Exception ex)  //timeout
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "---", $"Getmasterclps: {ex.Message}");
                return null;
            }
        }



        static public bool LinkToMob(AssetDescriptionType Asset, string PathURI)
        {
            string DispayName = Asset.Attributes.FirstOrDefault(x => x.Name == "Display Name").Value + "Link to Mob";

            LinkToMOBType LinkToMob = new LinkToMOBType() { InterplayMOBURI = Asset.InterplayURI, InterplayPathURI = PathURI };
            LinkToMOBResponseType LinkToMobResponse = _client.LinkToMOB(_user, LinkToMob);
            if (LinkToMobResponse.Errors == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public bool MoveAsset(string InterplayPath, string FolderUri)
        {
            // MoveType MoveAsset = new MoveType() { InterplayAssetURI = PathURI, InterplayFolderURI = FolderURI };
            MoveType AssetToMove = new MoveType() { InterplayAssetURI = "interplay://IP_WS_WG" + InterplayPath, InterplayFolderURI = "interplay://IP_WS_WG" + FolderUri };
            MoveResponseType MoveResponse = _client.Move(_user, AssetToMove);
            if (MoveResponse.Errors == null)
            {
                return true;
            }
            else
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Unable to move {InterplayPath} to {FolderUri} , {MoveResponse.Errors[0].Message}");
                return false;
            }
        }


        static public bool FindLinks(AssetDescriptionType Asset)
        {
            FindLinksType FindLinks = new FindLinksType() { InterplayURI = Asset.InterplayURI };
            FindLinksResponseType FindLinksResponse = _client.FindLinks(_user, FindLinks);
            if (FindLinksResponse.Errors == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        static public bool GetAttributes(string MobId)
        {

            AttributeType[] attributelist = new AttributeType[] {
                new AttributeType() {Group = "SYSTEM", Name = "Record Complete" },
                new AttributeType() {Group = "SYSTEM", Name = "Media Status", },
                new AttributeType() {Group = "SYSTEM", Name = "Path" },
                new AttributeType() {Group = "SYSTEM", Name = "Media Status" },
                new AttributeType() {Group = "USER", Name = "Video ID" },
            };
            GetAttributesType GetAttributes = new GetAttributesType() { InterplayURIs = new string[] { MobId }, Attributes = attributelist };
            GetAttributesResponseType GetAttributeResponse = _client.GetAttributes(_user, GetAttributes);
            if (GetAttributeResponse.Errors == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static public List<Locator> GetUmidLocators(string MobID)
        {
            GetLocatorsType GetLocator = new GetLocatorsType() { InterplayURI = "interplay://IP_WS_WG?mobid=" + MobID };
            GetUMIDLocatorsResponseType GetLocatorResponse = _client.GetUMIDLocators(_user, GetLocator);

            if (GetLocatorResponse.Errors == null)
            {
                List<Locator> locatorList = new List<Locator>();
                foreach (UMIDLocatorType locatorType in GetLocatorResponse.Results)
                {
                    Locator locator = new Locator();
                    locator.Comment = locatorType.Comment;
                    locator.TimeCode = locatorType.Timecode;
                    locator.Color = locatorType.Color;
                    locator.Track = locatorType.Track;
                    locator.UserName = locatorType.Username;
                    locator.LocatorUri = locatorType.LocatorURI;
                    locator.FramensFromStart = locatorType.FrameNumber;
                    locatorList.Add(locator);
                }
                return locatorList;
            }
            else
            {
                return new List<Locator>();
            }
        }

        static public string SaveUmidLocator(List<Locator> NewLocators, string MobID)
        {

            List<UMIDLocatorType> UmidLocators = new List<UMIDLocatorType>();
            foreach (Locator newLocator in NewLocators)
            {
                UMIDLocatorType locator = new UMIDLocatorType() { FrameNumber = newLocator.FramensFromStart, Color = newLocator.Color, Comment = newLocator.Comment, Track = newLocator.Track, Username = newLocator.UserName, LocatorURI = newLocator.LocatorUri, FrameNumberSpecified = true };
                UmidLocators.Add(locator);

            }
            SaveUMIDLocatorsType saveUmidLocators = new SaveUMIDLocatorsType() { Locators = UmidLocators.ToArray(), InterplayURI = "interplay://IP_WS_WG?mobid=" + MobID };

            SaveUMIDLocatorsResponseType response = _client.SaveUMIDLocators(_user, saveUmidLocators);
            if (response.Errors == null)
            {
                return "";
            }
            else
            {
                return $"Message: {response.Errors[0].Message} Details: {response.Errors[0].Details}";
            }
        }

        static public string DeleteUmidLocators(List<Locator> ObsoleteLocators, string MobID)
        {
            List<string> LocatorURIList = new List<string>();
            foreach (Locator locator in ObsoleteLocators)
            {
                LocatorURIList.Add(locator.LocatorUri);
            }

            RemoveUMIDLocatorsType RemoveUmidLocators = new RemoveUMIDLocatorsType() { InterplayURI = "interplay://IP_WS_WG?mobid=" + MobID, LocatorURIs = LocatorURIList.ToArray() };
            RemoveLocatorsResponseType response = _client.RemoveUMIDLocators(_user, RemoveUmidLocators);
            if (response.Errors == null)
            {
                return "";
            }
            else
            {
                return $"Message: {response.Errors[0].Message} Details: {response.Errors[0].Details}";
            }
        }



        static public void UpdateBackupStatus(string MobID)
        {
            var attributes = new SetAttributesType();
            attributes.InterplayURIs = new string[] { MobID };
            attributes.Attributes = new AttributeType[]
            {
                new AttributeType(){ Group="USER", Name="iNOS_Clip_Backup_Status", Value = "True"},
            };

            var response = _client.SetAttributes(_user, attributes);
            if (response.Errors == null)
            {
                //        Logger.WriteConsoleAndLog(MsgType.INFO, "---", $"UpdateMultiAttributes: {asset.Assetdata.AssetName}");
            }
            else
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "---", $"UpdateMultiAttributes: {JsonConvert.SerializeObject(response, Formatting.Indented)}");
            }
        }

        static public long GetFileDetails(string MobID)
        {
            var details = new GetFileDetailsType() { InterplayURIs = new string[] { "interplay://IP_WS_WG?mobid=" + MobID } };
            var response = _client.GetFileDetails(_user, details);
            if (response.Errors == null)
            {
                FileLocationDetailsType fdresponse = response.Results[0];
                long size = 0;
                foreach (FileLocationType flType in fdresponse.FileLocations)
                {
                    size = size + Convert.ToInt64(flType.Size) * 1024;
                }
                return size;
            }
            else
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "---", $"GetFileDetails: {JsonConvert.SerializeObject(response, Formatting.Indented)}");
                return 0;
            }
        }

        static public void DeeteAsset(string MobID)
        {
            var delete = new DeleteAssetsType()
            {
                DeleteMedia = true,
                DeleteMetadata = false,
                Resolutions = new string[] { "Data" },
                InterplayURIs = new string[] { "interplay://IP_WS_WG/" + MobID },
                DeleteMetadataSpecified = true,
            };

            var response = _client.DeleteAssets(_user, delete);
            if (response.Errors == null)
            {
            }
            else
            {

            }
        }

        static public void Getresolutions(string MobID)
        {
            var resolutions = new GetResolutionsType()
            {
                InterplayURIs = new string[] { "interplay://IP_WS_WG?mobid=" + MobID },
            };

            var response = _client.GetResolutions(_user, resolutions);
            if (response.Errors == null)
            {
            }
            else
            {

            }

        }

        static public void CreateSequences()
        {


            var attributes = new AttributeType[]
            {
                new AttributeType(){ Group="USER", Name="Video ID", Value = "MyVideoID"},
            };


            var ShotlistElement = new ShotlistElementType()
            {
                InterplayURI = "interplay://IP_WS_WG?mobid=" + "060a2b340101010101010f0013-000000-101e0800bfd0001f-060e2b347f7f-d080",
                InTimecode = "00:00:00:00",
                OutTimecode = "00:00:10:00"
            };
            var Shotlist = new CreateShotlistType()
            {
                DestinationFolderURI = "interplay://IP_WS_WG" + "/Catalogs/Scripts",
                ShotlistElements = new ShotlistElementType[] { ShotlistElement },
                ShotlistName = "ShotlistNaam",
                StartTimecode = "00:00:00:00",
                Attributes = attributes,
            };
            var response = _client.CreateShotlist(_user, Shotlist);
            if (response.Errors == null)
            {
            }
            else
            {

            }

        }
    }
}
