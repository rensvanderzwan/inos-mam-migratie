﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using static SharedLib.ShellProcess;

namespace SharedLib
{
    interface IShellProcess
    {
        string Command { get; set; }
        string Arguments { get; set; }
        Process Process { get; set; }
        StringBuilder Output { get; set; }
        StringBuilder Error { get; set; }
        bool TimeOut { get; set; }
        void Execute(int MaxRunMinutes, ConsoleCtrlEvent Event, bool Debug);
    }
}
