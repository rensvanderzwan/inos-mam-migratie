﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

// // sc create "iNOS-MAM-Migratie IngestIntoAvidHlv" binpath="C:\Program Files\Archiefmigratie IngestIntoAvidHlv\WindowsService IngestAssetIntoAvid.exe"

namespace WindowsService_IngestAssetIntoAvid
{
    partial class ServiceStarter : ServiceBase
    {
        private IngestAssetIntoAvid.Main _ingestservice;

        public ServiceStarter(string StartupArg)
        {
            _ingestservice = new IngestAssetIntoAvid.Main();
        }

        protected override void OnStart(string[] args)
        {
            _ingestservice.TaskStart();
        }

        protected override void OnStop()
        {
            _ingestservice.TaskStop();
        }
    }
}
