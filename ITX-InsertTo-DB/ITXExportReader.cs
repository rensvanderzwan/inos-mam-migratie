﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SharedLib;

namespace iNOS_MAM_Migratie
{
    public static class ITXExportReader
    {
        static public List<Asset> ReadFile(DateTime FromDate, ITXLocation location)
        {
            string fileLocation;
            if(location == ITXLocation.hlv)
                fileLocation = @"\\hstore\contenthlv$\Div\ITX-exports\ITX-Cliplist-HILV.csv";
            else
                fileLocation = @"\\dstore\contentdhg$\Div\ITX-exports\ITX-Cliplist-DHG.csv";

            string[] itxFile;
            try
            {
                itxFile = File.ReadAllLines(fileLocation);
            }
            catch(IOException ex)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, $"Readlines {fileLocation}", $"{ex.Message} { ex.InnerException}");
                return null;
            }

            
            List<string> itxLines = new List<string>(itxFile);
            if(itxLines.Count() < 140000)
            {
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Check", $"There are only {itxLines.Count()} in the export list {fileLocation}");
                return null;
            }

            List<Asset> itxAssets = new List<Asset>();
            foreach (string line in itxLines)
            {
                string[] elements = line.Split("\t");
                if(elements.Length == 7)
                {
                    Asset itxobject = MakeITXObject(elements);
                    itxAssets.Add(itxobject);
                }
                else
                {
                    Logger.WriteConsoleAndLog(MsgType.ERROR, null, $"Line has no 7 elements: {line}");
                    // once where here there must be something seriousely wrong , return null
                }
            }

            List<Asset> distinctItxAssets = itxAssets.GroupBy(x => x.AssetID).Select(y => y.First()).ToList();

            List<Asset> filteredAssets;
            if (location == ITXLocation.hlv)
                filteredAssets = FilterHLVAssets(FromDate, distinctItxAssets, fileLocation);
            else
                filteredAssets = FilterDHGAssets(FromDate, distinctItxAssets, fileLocation);

            return filteredAssets;
        }

        static private List<Asset> FilterHLVAssets(DateTime FromDate, List<Asset> distinctItxAssets, string filePath)
        {
            // build some filters based on contenttype.
            System.Text.RegularExpressions.Regex archiveRegEx = new System.Text.RegularExpressions.Regex("C.A");
            System.Text.RegularExpressions.Regex sportingestRegEx = new System.Text.RegularExpressions.Regex("CSI");
            System.Text.RegularExpressions.Regex sportTapeingestRegEx = new System.Text.RegularExpressions.Regex("CST");
            System.Text.RegularExpressions.Regex schoneInlasingestRegEx = new System.Text.RegularExpressions.Regex("C.G");
            System.Text.RegularExpressions.Regex kelderArchiefRegEx = new System.Text.RegularExpressions.Regex("CSK");
            System.Text.RegularExpressions.Regex doodArchiefRegEx = new System.Text.RegularExpressions.Regex("CSD");
    //        System.Text.RegularExpressions.Regex nieuwsuurIngestRegEx = new System.Text.RegularExpressions.Regex("CUI");


            List<Asset> archiveAssets = distinctItxAssets.Where(x => archiveRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> sportIngestAssets = distinctItxAssets.Where(x => sportingestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> schoneInlasIngestAssets = distinctItxAssets.Where(x => schoneInlasingestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> kelderArchiefAssets = distinctItxAssets.Where(x => kelderArchiefRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> doodArchiefAssets = distinctItxAssets.Where(x => doodArchiefRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> sportTapeIngestAssets = distinctItxAssets.Where(x => sportTapeingestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
    //        List<Asset> nieuwsuurIngestAssets = distinctItxAssets.Where(x => nieuwsuurIngestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();

            List<Asset> ContentTypeFilteredAssets = new List<Asset>();
            ContentTypeFilteredAssets.AddRange(archiveAssets);
            ContentTypeFilteredAssets.AddRange(sportIngestAssets);
            ContentTypeFilteredAssets.AddRange(schoneInlasIngestAssets);
            ContentTypeFilteredAssets.AddRange(kelderArchiefAssets);
            ContentTypeFilteredAssets.AddRange(doodArchiefAssets);
            ContentTypeFilteredAssets.AddRange(sportTapeIngestAssets);
    //        ContentTypeFilteredAssets.AddRange(nieuwsuurIngestAssets);

            List<Asset> DateFilteredAssets = ContentTypeFilteredAssets.Where(x => x.Assetdata.LastModified >= FromDate).ToList();

            // Now order the remainder of the list with oldest first
            List<Asset> DateOrderedFilteredAssets = DateFilteredAssets.OrderBy(t => t.Assetdata.LastModified).ToList();

            Logger.WriteConsoleAndLog(MsgType.INFO, "ReadFile", @$"Found {DateOrderedFilteredAssets.Count()} in {filePath}");

            return DateOrderedFilteredAssets;
        }

        static private List<Asset> FilterDHGAssets(DateTime FromDate, List<Asset> distinctItxAssets, string filePath)
        {
            // build some filters based on contenttype.
            System.Text.RegularExpressions.Regex IngestRegEx = new System.Text.RegularExpressions.Regex("C.I");
            System.Text.RegularExpressions.Regex PlayoutRegEx = new System.Text.RegularExpressions.Regex("C.P");
            System.Text.RegularExpressions.Regex OnlineRegEx = new System.Text.RegularExpressions.Regex("C.O");
            System.Text.RegularExpressions.Regex TapeingestRegEx = new System.Text.RegularExpressions.Regex("C.T");
     //       System.Text.RegularExpressions.Regex schoneInlasingestRegEx = new System.Text.RegularExpressions.Regex("C.G");


         //   List<Asset> archiveAssets = distinctItxAssets.Where(x => archiveRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
         //   List<Asset> sportIngestAssets = distinctItxAssets.Where(x => sportingestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
         //   List<Asset> schoneInlasIngestAssets = distinctItxAssets.Where(x => schoneInlasingestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
        //    List<Asset> kelderArchiefAssets = distinctItxAssets.Where(x => kelderArchiefRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
        //    List<Asset> doodArchiefAssets = distinctItxAssets.Where(x => doodArchiefRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> IngestAssets = distinctItxAssets.Where(x => IngestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> PlayoutIngestAssets = distinctItxAssets.Where(x => PlayoutRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> OnlineIngestAssets = distinctItxAssets.Where(x => OnlineRegEx.IsMatch(x.Assetdata.ContentType)).ToList();
            List<Asset> TapeIngestAssets = distinctItxAssets.Where(x => TapeingestRegEx.IsMatch(x.Assetdata.ContentType)).ToList();

            List<Asset> ContentTypeFilteredAssets = new List<Asset>();
            ContentTypeFilteredAssets.AddRange(IngestAssets);
            ContentTypeFilteredAssets.AddRange(PlayoutIngestAssets);
            ContentTypeFilteredAssets.AddRange(OnlineIngestAssets);
            ContentTypeFilteredAssets.AddRange(TapeIngestAssets);

            List<Asset> DateFilteredAssets = ContentTypeFilteredAssets.Where(x => x.Assetdata.LastModified >= FromDate).ToList();

            // Now order the remainder of the list with oldest first
            List<Asset> DateOrderedFilteredAssets = DateFilteredAssets.OrderBy(t => t.Assetdata.LastModified).ToList();

            Logger.WriteConsoleAndLog(MsgType.INFO, "ReadFile", @$"Found {DateOrderedFilteredAssets.Count()} in \\hstore\contenthlv$\Div\ITX-exports\ITX-Cliplist-HILV.csv");

            return DateOrderedFilteredAssets;
        }


            static private Asset MakeITXObject(string[] elements)
        {
            bool result = DateTime.TryParse(elements[5], out DateTime ModifiedDate);
            if ( result == false)
            {
                ModifiedDate = new DateTime(1969, 3, 1, 3, 15, 0);
                Logger.WriteConsoleAndLog(MsgType.ERROR, "Parse Export", $"Unable to extract modification date for asset: {elements[1]}");
            }
            Asset asset = new Asset();

            AssetData itxobject = new AssetData()
            {
                AssetID = Convert.ToInt64(elements[0]),
                AssetName = elements[1],
                ContentType = elements[2],
                LastModified = ModifiedDate.ToUniversalTime(),  // all in GMT/UTC
                IsPlaceholder = elements[6].Length == 0
            };

            asset.Assetdata = itxobject;
            asset.AssetID = itxobject.AssetID;

            return asset;
        }
    }
}
