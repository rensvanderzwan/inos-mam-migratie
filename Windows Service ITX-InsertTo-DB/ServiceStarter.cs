﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

// Install the service:
// sc create "iNOS-MAM-Migratie ITX-to-DB" binpath="C:\temp\ArchiefMigratie ITX-InsertTo-DB\Windows Service ITX-InsertTo-DB.exe"

namespace Windows_Service_ITX_InsertTo_DB
{
    partial class ServiceStarter : ServiceBase
    {
        private iNOS_MAM_Migratie.Main _importService;

        public ServiceStarter(string StartupArg)
        {
            _importService = new iNOS_MAM_Migratie.Main();
        }

        protected override void OnStart(string[] args)
        {
            _importService.TaskStart();
        }

        protected override void OnStop()
        {
            _importService.TaskStop();
        }
    }
}
