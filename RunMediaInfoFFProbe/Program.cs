﻿using System;

namespace RunMediaInfoFFProbe
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = new Main();
            result.Start(args[0]);
        }
    }
}
